import requests, os
from dotenv import load_dotenv
from pydantic import parse_obj_as
from typing import List
from dtos.data_balancing_dto import DataBalancingDto
from dtos.dropbox_file_dto import DropboxUploadResultDto
from dtos.model_configuration_dto import ModelConfigurationDto
from dtos.result_rules_dto import ResultDto
from dtos.task_dto import UpdateTaskMetaDataDto, TaskStatus
from dtos.prefix_data_dto import PrefixDataDto
from dtos.prefix_analitics_dto import PrefixAnaliticsDto


from models.model_configuration_model import ModelConfigurationModel
from models.column_configuration_model import ColumnConfigurationModel
from models.rule_configuration_model import RuleConfigurationModel

load_dotenv()
FAST_API_ENDPOINT = os.getenv('BACK_END_URL')

class IntegrationService():
    def post_main_file_upload_results(self, project_id:str, request_dto:DropboxUploadResultDto):
        end_point = FAST_API_ENDPOINT + 'uploadprojectfile/{0}/filemetadata'.format(project_id)

        if request_dto is not None:
            requests.post(end_point, data=request_dto.json())

    def post_main_file_columns(self, project_id:str, column_names:List[str]):
        end_point = FAST_API_ENDPOINT + 'uploadprojectfile/{0}/columnmetadata'.format(project_id)
        if len(column_names) > 0:
            requests.post(end_point, json=column_names)

    def update_task_precentage(self, task_id:str, process_precentage:float):
        self.update_task(UpdateTaskMetaDataDto(id = task_id, task_status = TaskStatus.PROCESSING, task_process_percentage = process_precentage))

    def task_done(self, task_id:str):
        self.update_task(UpdateTaskMetaDataDto(id = task_id, task_status = TaskStatus.DONE, task_process_percentage = 100.0))

    def task_faild(self, task_id:str):
        self.update_task(UpdateTaskMetaDataDto(id = task_id, task_status = TaskStatus.FAILED, task_process_percentage = 100.0))

    def update_task(self, request_dto:UpdateTaskMetaDataDto):
        end_point = FAST_API_ENDPOINT + 'task/'
        if request_dto is not None:
            requests.put(end_point, data=request_dto.json())

    def get_columns(self, configuration_id:str):
        end_point = FAST_API_ENDPOINT + 'columnmetadata/' + configuration_id
        result_json = requests.get(end_point).json()
        return parse_obj_as(List[ColumnConfigurationModel], result_json)

    def get_rules(self, configuration_id:str):
        end_point = FAST_API_ENDPOINT + 'ruleconfiguration/' + configuration_id
        result_json = requests.get(end_point).json()
        return parse_obj_as(List[RuleConfigurationModel], result_json)

    def post_prefix_data(self, prefix_data_dto:PrefixDataDto):
        end_point = FAST_API_ENDPOINT + 'prefixdata/'
        if prefix_data_dto is not None:
            requests.post(end_point, data=prefix_data_dto.json())

    def get_prefix_list_json(self, model_configuration_id:str):
        end_point = FAST_API_ENDPOINT + 'prefixdata/' + model_configuration_id
        return requests.get(end_point).json()

    def post_prefix_analitics(self, prefix_analitics_dto:PrefixAnaliticsDto):
        end_point = FAST_API_ENDPOINT + 'prefixanalitics/'
        if prefix_analitics_dto is not None:
            requests.post(end_point, data=prefix_analitics_dto.json())

    def get_model_config(self, model_configuration_id:str):
        end_point = FAST_API_ENDPOINT + 'modelconfiguration/' + model_configuration_id
        result_json = requests.get(end_point).json()
        return ModelConfigurationDto.parse_obj(result_json)

    def update_model_configuration(self, model_configuration:ModelConfigurationDto):
        end_point = FAST_API_ENDPOINT + 'modelconfiguration/'
        requests.put(end_point, data=model_configuration.json())

    def update_data_balance(self, model_configuration_id:str,  data_balance_dto:DataBalancingDto):
        end_point = FAST_API_ENDPOINT + 'modelconfiguration/' + model_configuration_id + '/databalance'
        requests.put(end_point, data=data_balance_dto.json())

    def post_result_rules(self, result_dto:ResultDto):
        end_point = FAST_API_ENDPOINT + 'resultrules/'
        requests.post(end_point, data=result_dto.json())