import os, uuid
from services.dropbox_service import DropBoxService, CHUNK_SIZE
from worker.services.integration_service import IntegrationService
from dropbox.files import UploadSessionCursor, CommitInfo
from dtos.dropbox_file_dto import DropboxUploadResultDto
from dtos.task_dto import UpdateTaskMetaDataDto, TaskStatus
from models.file_data_model import FileStatus
from datetime import datetime

dropbox_service = DropBoxService()
integration_service = IntegrationService()

def upload_file(folder_id:str, file_path:str, task_id:str):
    if os.path.exists(file_path):
        with open(file_path, 'rb') as gzip_file:
            file_size = os.path.getsize(file_path)

            dropbox_file_path = folder_id + '/' + str(uuid.uuid4()) + '.csv.gz'

            if file_size <= CHUNK_SIZE:
                return dropbox_service.upload_small_file(gzip_file, dropbox_file_path)
            else:
                upload_session = dropbox_service.start_large_file_upload_session(gzip_file)
                cursor = UploadSessionCursor(session_id=upload_session.session_id, offset=gzip_file.tell())
                commit = CommitInfo(path=dropbox_file_path)

                processed_size = 0
                while gzip_file.tell() < file_size:
                    processed_size += CHUNK_SIZE
                    update_task(task_id, processed_size, file_size)
                    if ((file_size - gzip_file.tell()) <= CHUNK_SIZE):
                        return dropbox_service.end_large_file_upload(gzip_file, cursor, commit)
                    else:
                        dropbox_service.upload_file_chunk(gzip_file, cursor)
                        cursor.offset = gzip_file.tell()


def update_task(task_id:str, processed_size:int, file_size:int):
    integration_service.update_task(
        UpdateTaskMetaDataDto(
            id = task_id,
            task_status = TaskStatus.PROCESSING,
            task_process_percentage = round(processed_size/file_size * 100.0, 2)
    ))

def build_upload_result(file_path:str, upload_result):
    request_dto = DropboxUploadResultDto()
    
    if upload_result is not None:
        request_dto.dropbox_id = upload_result.id
        request_dto.sync_date = datetime.now()
        request_dto.file_size = round(os.path.getsize(file_path) / 1024 / 1024, 2)
        request_dto.file_status = FileStatus.UPLOADED

    return request_dto

def download_file(local_path:str, file_dropbox_id:str):
    result = dropbox_service.download_file(local_path, file_dropbox_id)
    return result