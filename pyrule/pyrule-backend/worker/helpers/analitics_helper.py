from typing import List
from dtos.prefix_analitics_dto import PrefixAnaliticsDto, PrefixAnaliticsRowDto
from dtos.prefix_data_dto import PrefixRowDto


def build_prefix_analitics(prefix_list:List[PrefixRowDto]):
    prefix_analitics = PrefixAnaliticsDto()

    case_size_analitics_map = {}
    case_treatment_analitics_map = {}

    for prefix_row in prefix_list:
        add_value_to_map(case_size_analitics_map, prefix_row, prefix_row.case_size)

        if prefix_row.case_prefix_size != 0:
            add_value_to_map(case_treatment_analitics_map, prefix_row, prefix_row.case_prefix_size)

    prefix_analitics.case_size_analitics = list(sorted(case_size_analitics_map.values(), key=lambda d: d.index))
    prefix_analitics.case_treatment_analitics = list(sorted(case_treatment_analitics_map.values(), key=lambda d: d.index))

    return prefix_analitics


def add_value_to_map(analitics_map, prefix_row:PrefixRowDto, key:int):
    value = None
    if key in analitics_map:
        value = analitics_map[key]
    else:
        value = PrefixAnaliticsRowDto(index=key, has_treatment=0, missing_treatment = 0, success=0, failed=0)
        analitics_map[key] = value
    add_counter_to_prefix_analitics_row(prefix_row, value)

def add_counter_to_prefix_analitics_row(prefix_row:PrefixRowDto, value:PrefixAnaliticsRowDto):
    if prefix_row.is_successful:
        value.success += 1
    else:
        value.failed += 1
    if prefix_row.has_treatment:
        value.has_treatment +=1
    else:
        value.missing_treatment += 1