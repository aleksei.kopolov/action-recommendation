import os, gzip, shutil, csv
import pandas as pd

def get_csv_columns(file_path:str):
    column_names = []

    df = get_csv_file_dataframe(file_path)

    if df is not None:
        column_names = df.columns.tolist()
    
    return column_names

def get_csv_file_dataframe(file_path:str):
    df = None

    if os.path.exists(file_path):
        with open(file_path, 'r') as csv_file:
            dialect = csv.Sniffer().sniff(csv_file.readline())
            df = pd.read_csv(file_path, index_col=False, nrows=0, delimiter=dialect.delimiter)

    return df

def get_gzip_file_dataframe(file_path:str):
    df = None

    if os.path.exists(file_path):
        df = pd.read_csv(file_path, index_col=False)

    return df

def compress_and_remove_file(file_path:str):
    gz_file_path = file_path + '.gz'

    if os.path.exists(file_path):
        with open(file_path, 'r') as csv_file:
            dialect = csv.Sniffer().sniff(csv_file.readline())
            df = pd.read_csv(file_path, index_col=False, delimiter=dialect.delimiter)
            df.to_csv(gz_file_path, compression='gzip', sep=',', index=False)

        remove_existing_file(file_path)

    return gz_file_path

def save_file(file_path:str, df:pd.DataFrame):
    df.to_csv(file_path, index=False)

def remove_existing_file(file_path:str):
    if os.path.exists(file_path):
        os.remove(file_path)