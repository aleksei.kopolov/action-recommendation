import os
from dotenv import load_dotenv

from celery import Celery
from dtos.result_rules_dto import ResultDto

from worker.helpers.dropbox_helper import upload_file, build_upload_result, download_file
from worker.helpers.file_helper import get_csv_columns, compress_and_remove_file, remove_existing_file, get_gzip_file_dataframe
from worker.helpers.analitics_helper import build_prefix_analitics
from worker.ml.model_builder import build_uplift_model, get_uplift_rules

from worker.services.integration_service import IntegrationService

from worker.ml.obj.input_meta_data import InputMetaData
from worker.ml.prefix_processer import build_prefix_mask, get_balanced_prefix_mask, extract_data_from_main_df, build_confusion_matrix
from worker.ml.helpers.pandas_helper import prepare_data_frame_for_processing, remove_unused_columns, set_data_types

from dtos.prefix_data_dto import PrefixDataDto

load_dotenv()
celery = Celery('prolift', broker=os.getenv('MONGO_DB_ULR'), backend=os.getenv('MONGO_DB_ULR'))

integration_service = IntegrationService()

@celery.task
def compress_file(file_path:str):
    return compress_and_remove_file(file_path)

@celery.task
def upload_main_file(file_path:str, dropbox_id:str, project_id:str, task_id:str):
    upload_result = upload_file(dropbox_id, file_path, task_id)
    request_dto = build_upload_result(file_path, upload_result)
    if request_dto is not None:
        integration_service.post_main_file_upload_results(project_id, request_dto)
        integration_service.task_done(task_id)
    return file_path

@celery.task
def extract_main_file_columns(file_path:str, project_id:str):
    column_names = get_csv_columns(file_path)
    integration_service.post_main_file_columns(project_id, column_names)
    return file_path

@celery.task
def delete_temp_file(file_path:str):
    if file_path is not None:
        remove_existing_file(file_path)

@celery.task
def download_main_file(local_path:str, file_dropbox_id:str, task_id:str):
    try:
        result = download_file(local_path, file_dropbox_id)
        if result is None:
            integration_service.task_faild(task_id)
        else:
            integration_service.task_done(task_id)
    except:
        integration_service.task_faild(task_id)

@celery.task
def preprocess_file(local_path:str, model_configuration_id:str, task_id:str):
    try:
        model_configuration = integration_service.get_model_config(model_configuration_id)
        input_meta_data = InputMetaData(model_configuration)

        df = get_gzip_file_dataframe(local_path)
        df = prepare_data_frame_for_processing(df=df, input_meta_data=input_meta_data)
        
        prefix_list = build_prefix_mask(df, input_meta_data, task_id)
        prefix_dto = PrefixDataDto(model_configuration_id = model_configuration_id, prefix_list=prefix_list)
        integration_service.post_prefix_data(prefix_dto);

        prefix_analitics = build_prefix_analitics(prefix_list)
        prefix_analitics.model_configuration_id = model_configuration_id
        integration_service.post_prefix_analitics(prefix_analitics)

        integration_service.task_done(task_id)
    except:
        integration_service.task_faild(task_id)

@celery.task
def balance_data(model_configuration_id:str, task_id:str):
    try:
        model_configuration = integration_service.get_model_config(model_configuration_id)
        prefix_list_json = integration_service.get_prefix_list_json(model_configuration_id)
        model_configuration.data_balancing.confusion_matrix = build_confusion_matrix(prefix_list_json, model_configuration).tolist()
        integration_service.update_data_balance(model_configuration_id, model_configuration.data_balancing)
        integration_service.task_done(task_id)
    except:
        integration_service.task_faild(task_id)

@celery.task
def build_model(local_path:str, model_configuration_id:str, task_id:str):
    try:
        model_configuration = integration_service.get_model_config(model_configuration_id)
        input_meta_data = InputMetaData(model_configuration)

        prefix_list_json = integration_service.get_prefix_list_json(model_configuration_id)
        balanced_prefix_mask_df = get_balanced_prefix_mask(prefix_list_json, model_configuration)

        df = get_gzip_file_dataframe(local_path)
        df = prepare_data_frame_for_processing(df, input_meta_data)
        df = extract_data_from_main_df(df, balanced_prefix_mask_df, input_meta_data)
        df = remove_unused_columns(df, input_meta_data)
        df = set_data_types(df, input_meta_data)
        uplift_model = build_uplift_model(df, model_configuration)
        result_rules = get_uplift_rules(uplift_model, df, input_meta_data, model_configuration_id)
        result_rule_dto = ResultDto(result_rules=result_rules)
        result_rule_dto.model_configuration_id = model_configuration_id
        integration_service.post_result_rules(result_rule_dto)
        integration_service.task_done(task_id)
    except:
        integration_service.task_faild(task_id)
