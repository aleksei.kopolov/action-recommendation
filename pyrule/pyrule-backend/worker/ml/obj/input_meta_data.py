from typing import List
from dtos.column_configuration_dto import ColumnConfigurationDto
from dtos.model_configuration_dto import ModelConfigurationDto

from models.column_configuration_model import ColumnType, ColumnDataType
from worker.ml.helpers.query_helper import build_pandas_query

class InputMetaData():
    def __init__(self, model_configuration:ModelConfigurationDto):
        self.set_data_types(model_configuration.column_configurations)
        self.result_rules = model_configuration.result_rules
        self.treatment_rules = model_configuration.treatment_rules
        self.set_queries()

    def set_data_types(self, columns:List[ColumnConfigurationDto]):
        self.column_name_by_type = {ColumnType.IGNORE : [], ColumnType.OUTCOME : [], ColumnType.GROUP_IDENTIFIER : [], ColumnType.ORDER_BY : [], ColumnType.UNCONTROLLABLE : [], ColumnType.CONTROLLABLE : []}
        self.column_name_by_data_type = {ColumnDataType.TEXT : [], ColumnDataType.NUMBER : [], ColumnDataType.BOOLEAN : [], ColumnDataType.DATE : [], ColumnDataType.CATEGORICAL : []}
        for column in columns:
            self.set_column_type(column)
            self.set_column_data_type(column)

    def set_column_type(self, column:ColumnConfigurationDto):
        if column.column_type == ColumnType.IGNORE:
            self.column_name_by_type[ColumnType.IGNORE].append(column.column_name)
        elif column.column_type == ColumnType.OUTCOME:
            self.column_name_by_type[ColumnType.OUTCOME].append(column.column_name)
        elif column.column_type == ColumnType.GROUP_IDENTIFIER:
            self.column_name_by_type[ColumnType.GROUP_IDENTIFIER].append(column.column_name)
        elif column.column_type == ColumnType.ORDER_BY:
            self.column_name_by_type[ColumnType.ORDER_BY].append(column.column_name)
        elif column.column_type == ColumnType.UNCONTROLLABLE:
            self.column_name_by_type[ColumnType.UNCONTROLLABLE].append(column.column_name)
        elif column.column_type == ColumnType.CONTROLLABLE:
            self.column_name_by_type[ColumnType.CONTROLLABLE].append(column.column_name)        
        else:
            self.column_name_by_type[ColumnType.IGNORE].append(column.column_name)

    def set_column_data_type(self, column:ColumnConfigurationDto):
        if column.column_data_type == ColumnDataType.TEXT:
            self.column_name_by_data_type[ColumnDataType.TEXT].append(column.column_name)
        elif column.column_data_type == ColumnDataType.NUMBER:
            self.column_name_by_data_type[ColumnDataType.NUMBER].append(column.column_name)
        elif column.column_data_type == ColumnDataType.BOOLEAN:
            self.column_name_by_data_type[ColumnDataType.BOOLEAN].append(column.column_name)
        elif column.column_data_type == ColumnDataType.DATE:
            self.column_name_by_data_type[ColumnDataType.DATE].append(column.column_name)
        elif column.column_data_type == ColumnDataType.CATEGORICAL:
            self.column_name_by_data_type[ColumnDataType.CATEGORICAL].append(column.column_name)
        else:
            self.column_name_by_data_type[ColumnDataType.TEXT].append(column.column_name)

    def set_queries(self):
        self.result_queries = build_pandas_query(self.result_rules)
        self.treatment_queries = build_pandas_query(self.treatment_rules)
