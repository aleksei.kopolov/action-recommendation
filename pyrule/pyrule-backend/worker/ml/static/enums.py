from typing import List, Set, Dict, Tuple, Optional
from enum import Enum

class PrefixMask(Enum):
    GROUP_NAME = 'group_name'
    SUCCESS = 'is_successful'
    TREATMENT = 'has_treatment'
    CASE_START_INDEX = 'case_start_index'
    CASE_END_INDEX = 'case_end_index'
    PREFIX_CUT_OFF_INDEX = 'case_cutoff_index'
    CASE_SIZE = 'case_size'
    PREFIX_SIZE = 'case_prefix_size'

class CategoricalMetaData(Enum):
    COLUMN_NAME = 'column_name'
    ORIGINAL_VALUE = 'original_value'
    NEW_VALUE = 'new_value'
