import pandas as pd
import numpy as np
import time

from typing import List
from dtos.model_configuration_dto import ModelConfigurationDto
from worker.ml.obj.input_meta_data import InputMetaData
from models.column_configuration_model import ColumnType
from dtos.prefix_data_dto import PrefixRowDto
from worker.ml.static.enums import PrefixMask
from worker.services.integration_service import IntegrationService

integration_service = IntegrationService()

def build_prefix_mask(df:pd.DataFrame, input_meta_data:InputMetaData, task_id:str):
    prefix_mask = []

    dg = df.groupby(input_meta_data.column_name_by_type[ColumnType.GROUP_IDENTIFIER])
    tottal_count = len(dg)
    last_send = time.perf_counter()

    for idx, (group_name, group_df) in enumerate(dg):
        prefix_mask.append(proccess_each_group(input_meta_data, group_name, group_df))
        if time.perf_counter() - last_send >= 5:
            current_progress = round((idx/tottal_count) * 100, 2)
            integration_service.update_task_precentage(task_id, current_progress)
            last_send = time.perf_counter()

    return prefix_mask

def proccess_each_group(input_meta_data:InputMetaData, group_name:str, group_df : pd.DataFrame):
    prefix_row = PrefixRowDto()
    result_index_mask = process_queries(input_meta_data.result_queries, group_df)
    treatment_index_mask = process_queries(input_meta_data.treatment_queries, group_df)
    cutoff_index = get_cutoff_index(treatment_index_mask)
    group_has_treatment = cutoff_index != -1;

    prefix_row.group_name = group_name
    prefix_row.is_successful = is_result_successful(result_index_mask)
    prefix_row.has_treatment = group_has_treatment
    prefix_row.case_start_index = int(group_df.index[0])
    prefix_row.case_end_index = int(group_df.index[-1])
    prefix_row.case_size = int(group_df.index[-1] - group_df.index[0])
    prefix_row.case_cutoff_index = int(cutoff_index)

    if cutoff_index == -1:
        prefix_row.case_prefix_size = 0
    else:
        prefix_row.case_prefix_size = int(cutoff_index - group_df.index[0])

    del group_name

    return prefix_row

def is_result_successful(result_index_mask:List[List[int]]):
    if result_index_mask is None or len(result_index_mask) == 0:
        return False

    for result_group in result_index_mask:
        if result_group is None or len(result_group) == 0:
            return False

    return True
    
def get_cutoff_index(treatment_index_mask:List[List[int]]):
    last_treatment_index = -1

    if treatment_index_mask is None or len(treatment_index_mask) == 0:
        return last_treatment_index

    for treatmnet_group in treatment_index_mask:
        has_treatment = False

        if treatmnet_group is None or len(treatment_index_mask) == 0:
            return last_treatment_index

        for treatment_index in treatmnet_group:
            if treatment_index > last_treatment_index:
                last_treatment_index = treatment_index
                has_treatment = True
                break

        if has_treatment == False:
            return -1

    return last_treatment_index

def process_queries(query_set:List[str], group_df : pd.DataFrame):
    result_mask = []
    for query in query_set:
        result_mask.append(process_rules(query, group_df))

    return result_mask

def process_rules(query:str, group_df:pd.DataFrame):
    filtered_df = group_df.query(query)

    if filtered_df is None:
        return []
    else:
        return filtered_df.index.tolist()
        
def get_confusion_matrix(mask_df:pd.DataFrame):
    return (mask_df[[PrefixMask.SUCCESS.value, PrefixMask.TREATMENT.value]]
            .groupby([PrefixMask.SUCCESS.value, PrefixMask.TREATMENT.value])
            .size()
            .unstack(fill_value=0)
            .rename(columns={0 : 'False', 1 : 'True'}, index={0 : 'False', 1 : 'True'})
        ).to_numpy()

def is_mask_balanced(confusion_matrix:List[List[int]], balance_max_delta):
    flattened_matrix = confusion_matrix.flatten()
    flattened_matrix.sort()

    min_value = flattened_matrix[0]
    max_value = flattened_matrix[-1]

    if get_min_max_precentage(min_value, max_value) <= balance_max_delta:
        return True
    else:
        return False

def get_min_max_precentage(a, b):
    min_value = None
    max_value = None

    if a < b:
        min_value = a
        max_value = b
    else:
        min_value = b
        max_value = a

    return 1 - (min_value/max_value)

def get_balanced_confusion_matrix(confusion_matrix:List[List[int]], balance_max_delta):
    flattened_matrix = confusion_matrix.flatten()
    flattened_matrix.sort()

    min_value = flattened_matrix[0]

    for i in range(2):
        for j in range(2):
            selected_value = confusion_matrix[i][j]
            if get_min_max_precentage(min_value, selected_value) > balance_max_delta:
                confusion_matrix[i][j] = int(min_value * (1 + balance_max_delta))

    return confusion_matrix

def balance_mask(unbalanced_mask_df:pd.DataFrame, balanced_confusion_matrix):
        tn = balanced_confusion_matrix[0][0]
        fp = balanced_confusion_matrix[0][1]
        fn = balanced_confusion_matrix[1][0]
        tp = balanced_confusion_matrix[1][1]

        with_treatment = unbalanced_mask_df[unbalanced_mask_df[PrefixMask.TREATMENT.value] == True].sort_values(PrefixMask.CASE_SIZE.value)
        without_treatment = unbalanced_mask_df[unbalanced_mask_df[PrefixMask.TREATMENT.value] == False].sort_values(PrefixMask.CASE_SIZE.value)
        
        return pd.concat(
            [
                with_treatment[with_treatment[PrefixMask.SUCCESS.value] == True].head(tp),
                with_treatment[with_treatment[PrefixMask.SUCCESS.value] == False].head(fp),
                without_treatment[without_treatment[PrefixMask.SUCCESS.value] == True].head(fn),
                without_treatment[without_treatment[PrefixMask.SUCCESS.value] == False].head(tn)
            ]
        )

def build_confusion_matrix(prefix_list, model_configuration:ModelConfigurationDto):
    prefix_mask_df = pd.DataFrame(data=prefix_list)
    prefix_mask_df = prefix_mask_df[prefix_mask_df[PrefixMask.CASE_SIZE.value] <= prefix_mask_df[PrefixMask.CASE_SIZE.value].quantile(model_configuration.data_balancing.cut_off_percentile)]

    confusion_matrix = get_confusion_matrix(prefix_mask_df)    
    if is_mask_balanced(confusion_matrix, model_configuration.data_balancing.balance_max_delta):
        return confusion_matrix
    
    return get_balanced_confusion_matrix(confusion_matrix, model_configuration.data_balancing.balance_max_delta)


def get_balanced_prefix_mask(prefix_list, model_configuration:ModelConfigurationDto):
    prefix_mask_df = pd.DataFrame(data=prefix_list)
    prefix_mask_df = prefix_mask_df[prefix_mask_df[PrefixMask.CASE_SIZE.value] <= prefix_mask_df[PrefixMask.CASE_SIZE.value].quantile(model_configuration.data_balancing.cut_off_percentile)]

    # confusion_matrix = get_confusion_matrix(prefix_mask_df)

    # if is_mask_balanced(confusion_matrix, model_configuration.data_balancing.balance_max_delta):
    #     return prefix_mask_df

    # balanced_confusion_matrix = get_balanced_confusion_matrix(confusion_matrix, model_configuration.data_balancing.balance_max_delta)
    return balance_mask(prefix_mask_df, model_configuration.data_balancing.confusion_matrix)


def get_prefix_index_list(balanced_prefix_mask_df:pd.DataFrame):
    start_end_tuple = zip(balanced_prefix_mask_df[PrefixMask.CASE_START_INDEX.value], balanced_prefix_mask_df[PrefixMask.CASE_END_INDEX.value])
    index_list = np.array([np.arange(start, end) for start, end in start_end_tuple], dtype=object)
    return np.concatenate([x.ravel() for x in index_list])


def extract_data_from_main_df(df:pd.DataFrame, balanced_prefix_mask_df:pd.DataFrame, input_meta_data:InputMetaData):
    index_list = get_prefix_index_list(balanced_prefix_mask_df)
    index_list.sort()
    return (
        df.loc[index_list].merge(
            balanced_prefix_mask_df[[PrefixMask.GROUP_NAME.value, PrefixMask.SUCCESS.value, PrefixMask.TREATMENT.value]], 
            left_on=input_meta_data.column_name_by_type[ColumnType.GROUP_IDENTIFIER],
            right_on=PrefixMask.GROUP_NAME.value
        )
    )