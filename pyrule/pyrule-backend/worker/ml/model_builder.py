from typing import List
import pandas as pd
from dtos.model_configuration_dto import ModelConfigurationDto
from dtos.rule_configuration_dto import RuleDto
from dtos.result_rules_dto import ResultRuleDto
from models.column_configuration_model import ColumnDataType

from models.rule_model import Operator
from worker.ml.obj.input_meta_data import InputMetaData
from worker.ml.static.enums import PrefixMask
from causalml.inference.tree import UpliftTreeClassifier
from sklearn.model_selection import train_test_split


def build_uplift_model(df:pd.DataFrame, model_configuration:ModelConfigurationDto):
    df_train, df_test = train_test_split(df, test_size=0.2)

    uplift_model = UpliftTreeClassifier(
        max_depth=model_configuration.model_settings.max_depth, 
        min_samples_leaf=model_configuration.model_settings.min_samples_leaf, 
        min_samples_treatment=model_configuration.model_settings.min_samples_treatment, 
        n_reg=model_configuration.model_settings.n_reg, 
        evaluationFunction=model_configuration.model_settings.evaluationFunction.value, 
        control_name='Control'
    )
    uplift_model.fit(
        df_train.values,
        treatment=df_train[PrefixMask.TREATMENT.value].values,
        y=df_train[PrefixMask.SUCCESS.value].values)

    return uplift_model


def get_uplift_rules(uplift_model, df, input_meta_data, model_configuration_id):
    result_rules = []
    data_headers = get_data_frame_headers(df.columns.tolist())
    build_rules(uplift_model.fitted_uplift_tree, data_headers, [], result_rules, input_meta_data, model_configuration_id)
    return result_rules


def get_data_frame_headers(columns):
    dcHeadings = {}
    for i, szY in enumerate(columns + ['treatment_group_key']):
        szCol = i
        dcHeadings[szCol] = str(szY)
    return dcHeadings


def build_rules(decisionTree, data_headers, rules:List[RuleDto], result_rules:List[ResultRuleDto], input_meta_data:InputMetaData, model_configuration_id:str):
    if decisionTree.results is not None:  # leaf node
        if len(rules) > 0:
            result_rules.append(ResultRuleDto(uplift_score=decisionTree.summary['matchScore'], rules=rules, model_configuration_id=model_configuration_id))
    else:
        file_name = decisionTree.col
        if file_name in data_headers:
            file_name = data_headers[file_name]

        (true_rule, false_rule) = build_rule(file_name, decisionTree.value, input_meta_data)
        
        build_rules(decisionTree.trueBranch, data_headers, rules + [true_rule], result_rules, input_meta_data, model_configuration_id)
        build_rules(decisionTree.falseBranch, data_headers, rules + [false_rule], result_rules, input_meta_data, model_configuration_id)


def build_rule(file_name, value, input_meta_data:InputMetaData):
    categorical_columns = input_meta_data.column_name_by_data_type[ColumnDataType.CATEGORICAL]

    for categorical_column in categorical_columns:
        if categorical_column in file_name:
            value = file_name.replace(categorical_column + '_', '')
            file_name = categorical_column
            break

    if isinstance(value, int) or isinstance(value, float):
        true_rule = RuleDto(field=file_name, value=value, operator=Operator.LARGER_EQUAL)
        false_rule = RuleDto(field=file_name, value=value, operator=Operator.SMALLER)
    else:
        true_rule = RuleDto(field=file_name, value=value, operator=Operator.EQUAL)
        false_rule = RuleDto(field=file_name, value=value, operator=Operator.NOT_EQUAL)

    return (true_rule, false_rule)