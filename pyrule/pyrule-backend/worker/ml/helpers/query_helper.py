from typing import List
from dtos.rule_configuration_dto import RuleDto

from models.rule_model import Operator

def build_pandas_query(rule_sets:List[List[RuleDto]]):
    query_list = []

    for rule_set in rule_sets:
        query_list.append(process_rule(rule_set))

    return query_list


def process_rule(rule_set:List[RuleDto], rule_merge_string : str = ' & '):
    query_list = []
    for rule in rule_set:
        if rule.field != None and rule.value != None and rule.operator != None:
            query = ''
            query += '{0}'.format(rule.field)
            query += ' {0} '.format(get_operator_string_value(rule.operator))
            value_type = type(rule.value)
            if value_type is int or value_type is bool or rule.value == 'True' or rule.value == 'False':
                query += ' {0} '.format(rule.value)
            else:
                if '\'' == rule.value[0] or '"' == rule.value[0]:
                    query += '{0}'.format(rule.value)
                else:
                    query += '\'{0}\''.format(rule.value)
            query_list.append(query)

    return rule_merge_string.join(query_list)


def get_operator_string_value(operator:Operator):
    if operator == Operator.EQUAL:
        return '=='
    elif operator == Operator.NOT_EQUAL:
        return '!='
    elif operator == Operator.LARGER:
        return '>'
    elif operator == Operator.LARGER_EQUAL:
        return '>='
    elif operator == Operator.SMALLER:
        return '<'
    elif operator == Operator.SMALLER_EQUAL:
        return '<='