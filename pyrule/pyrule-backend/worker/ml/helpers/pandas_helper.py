import pandas as pd

from models.column_configuration_model import ColumnType, ColumnDataType
from worker.ml.obj.input_meta_data import InputMetaData
from worker.ml.static.enums import PrefixMask

def prepare_data_frame_for_processing(df:pd.DataFrame, input_meta_data:InputMetaData) -> pd.DataFrame:
    df = df.drop(columns=input_meta_data.column_name_by_type[ColumnType.IGNORE])
    return sort_data_frame(df=df, input_meta_data=input_meta_data)

def sort_data_frame(df:pd.DataFrame, input_meta_data:InputMetaData) -> pd.DataFrame:
    return (df
        .sort_values(by=input_meta_data.column_name_by_type[ColumnType.GROUP_IDENTIFIER] + input_meta_data.column_name_by_type[ColumnType.ORDER_BY])
        .reset_index(drop=True)
    )

def set_data_frame_types(df:pd.DataFrame, input_meta_data:InputMetaData) -> pd.DataFrame:
    df[input_meta_data.column_name_by_data_type[ColumnDataType.NUMBER]] = pd.to_numeric(df[input_meta_data.column_name_by_data_type[ColumnDataType.NUMBER]], errors='coerce')
    return df

def remove_unused_columns(df:pd.DataFrame, input_meta_data:InputMetaData) -> pd.DataFrame:
    valuable_columns = (
        input_meta_data.column_name_by_type[ColumnType.UNCONTROLLABLE] + 
        input_meta_data.column_name_by_type[ColumnType.CONTROLLABLE] + 
        input_meta_data.column_name_by_type[ColumnType.OUTCOME] + 
        [PrefixMask.TREATMENT.value, PrefixMask.SUCCESS.value])
    delete_columns = [col for col in list(df.columns) if col not in valuable_columns]
    return df.drop(columns=delete_columns)

def set_data_types(df:pd.DataFrame, input_meta_data:InputMetaData) -> pd.DataFrame:
    df = pd.get_dummies(data=df, columns=input_meta_data.column_name_by_data_type[ColumnDataType.CATEGORICAL])
    df[PrefixMask.TREATMENT.value] = df[PrefixMask.TREATMENT.value].map({False:'Control', True:'Treatment'})
    return df