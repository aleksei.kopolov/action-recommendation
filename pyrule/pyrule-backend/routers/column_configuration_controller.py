from typing import List
from fastapi import APIRouter
from models.column_configuration_model import ColumnConfigurationModel
from dtos.column_configuration_dto import ColumnConfigurationDto
from services.column_configuration_service import ColumnConfigurationService
from helpers.column_helper import update_column_configuration

column_configuration_service = ColumnConfigurationService()

router = APIRouter()

END_POINT = "/columnmetadata/"
TAG = "columnmetadata"

@router.get(END_POINT, tags=[TAG], response_model=List[ColumnConfigurationModel])
async def get_all():
    return await column_configuration_service.get_all()

@router.put(END_POINT, tags=[TAG])
async def update_column(column_configuration : ColumnConfigurationDto):
    await update_column_configuration(column_configuration)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    await column_configuration_service.delete_all()