from typing import List
from fastapi import APIRouter, File, UploadFile
from helpers.file_helper import create_project_file_metadata, update_file_data_after_dropbox_upload, upload_main_file_to_dropbox, update_porject_file_id
from helpers.project_info_helper import get_project_general_info
from helpers.column_helper import create_project_columns
from dtos.project_info_dto import ProjectInfoDto
from dtos.dropbox_file_dto import DropboxUploadResultDto

from services.file_data_service import FileDataService
from services.project_service import ProjectService

router = APIRouter()
project_service = ProjectService()
file_metadata_service = FileDataService()

TEMP_FOLDER = "/uploadprojectfile/"
TAG = "uploadprojectfile"

@router.post(TEMP_FOLDER + "{project_id}", tags=[TAG], response_model=ProjectInfoDto)
async def upload_project_file(project_id:str, upload_file: UploadFile = File(...)):
    project = await project_service.get_by_id(project_id)
    file_data = await create_project_file_metadata(upload_file.filename)
    await update_porject_file_id(project, file_data)
    await upload_main_file_to_dropbox(project, file_data, upload_file)
    return await get_project_general_info(project_id)

@router.post(TEMP_FOLDER + "{project_id}/filemetadata", tags=[TAG])
async def upload_file_metadata_result(project_id:str, upload_result_dto:DropboxUploadResultDto):
    await update_file_data_after_dropbox_upload(project_id, upload_result_dto)

@router.post(TEMP_FOLDER + "{project_id}/columnmetadata", tags=[TAG])
async def update_column_metadata(project_id:str, column_names:List[str]):
    await create_project_columns(project_id, column_names)