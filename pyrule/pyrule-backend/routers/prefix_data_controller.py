from typing import List
from fastapi import APIRouter

from dtos.prefix_data_dto import PrefixDataDto
from models.prefix_data_model import PrefixDataModel
from helpers.prefix_data_helper import save_prefix_data
from models.prefix_row_model import PrefixRowModel
from services.prefix_data_service import PrefixDataService

router = APIRouter()
prefix_data_service = PrefixDataService()

END_POINT = "/prefixdata/"
TAG = "prefixdata"


@router.get(END_POINT, tags=[TAG], response_model=List[PrefixDataModel])
async def get_all():
    return await prefix_data_service.get_all()

@router.get(END_POINT + "{model_configuration_id}", tags=[TAG], response_model=List[PrefixRowModel])
async def get_by_model_configuration_id(model_configuration_id:str):
    prefix_list = []
    prefix_data_model = await prefix_data_service.get_by_model_configuration_id(model_configuration_id)
    if prefix_data_model is not None:
        prefix_list = prefix_data_model.prefix_list
    return prefix_list

@router.post(END_POINT, tags=[TAG])
async def create(prefix_data_dto: PrefixDataDto):
    return await save_prefix_data(prefix_data_dto)

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete_by_id(id:str):
    return await prefix_data_service.delete_by_id(id)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    return await prefix_data_service.delete_all()