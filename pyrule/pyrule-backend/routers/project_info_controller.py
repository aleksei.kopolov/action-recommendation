from typing import List
from fastapi import APIRouter
from dtos.rule_configuration_dto import RuleConfigurationDto
from helpers.project_info_helper import get_project_general_info, get_project_columns_info, get_project_treatment_rules, get_project_result_rules, get_project_models, create_model_configuration
from dtos.project_info_dto import ProjectInfoDto, ProjectColumnsInfoDto
from dtos.project_model_dto import ProjectModelDto

router = APIRouter()

END_POINT = "/projectinfo/"
TAG = "projectinfo"

@router.get(END_POINT + "{project_id}", tags=[TAG], response_model=ProjectInfoDto)
async def get_project(project_id:str):
    return await get_project_general_info(project_id)

@router.get(END_POINT + "{project_id}/columns", tags=[TAG], response_model=ProjectColumnsInfoDto)
async def get_project_columns(project_id:str):
    return await get_project_columns_info(project_id)

@router.get(END_POINT + "{project_id}/treatmentrules", tags=[TAG], response_model=RuleConfigurationDto)
async def get_treatment_rules(project_id:str):
    return await get_project_treatment_rules(project_id)

@router.get(END_POINT + "{project_id}/resultrules", tags=[TAG], response_model=RuleConfigurationDto)
async def get_result_rules(project_id:str):
    return await get_project_result_rules(project_id)

@router.get(END_POINT + "{project_id}/models", tags=[TAG], response_model=List[ProjectModelDto])
async def get_models(project_id:str):
    return await get_project_models(project_id)

@router.post(END_POINT + "{project_id}/model", tags=[TAG], response_model=ProjectModelDto)
async def create_model(project_id:str, project_model_dto:ProjectModelDto):
    return await create_model_configuration(project_id, project_model_dto)