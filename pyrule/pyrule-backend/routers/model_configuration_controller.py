from typing import List
from fastapi import APIRouter
from dtos.data_balancing_dto import DataBalancingDto
from models.data_balancing_model import DataBalancingModel
from models.model_configuration_model import ModelConfigurationModel
from services.model_configuration_service import ModelConfigurationService
from services.task_service import TaskService
from dtos.model_configuration_dto import ModelConfigurationDto

router = APIRouter()
model_configuration_service = ModelConfigurationService()
task_data_service = TaskService()

END_POINT = "/modelconfiguration/"
TAG = "modelconfiguration"

@router.get(END_POINT, tags=[TAG], response_model=List[ModelConfigurationModel])
async def get_all():
    return await model_configuration_service.get_all()

@router.get(END_POINT + "{id}", tags=[TAG], response_model=ModelConfigurationDto)
async def get_by_id(id:str):
    task = await task_data_service.get_active_data_processing_task_by_related_id(id)
    model_configuration_dto = ModelConfigurationDto()
    model_configuration = await model_configuration_service.get_by_id(id)
    model_configuration_dto.set_data(model_configuration)
    if task is not None:
        model_configuration_dto.process.is_processing = True
        model_configuration_dto.process.process_percentage = task.task_process_percentage
        model_configuration_dto.process.status = task.task_status
    return model_configuration_dto

@router.get(END_POINT + "{id}/confusionmatrix", tags=[TAG], response_model=List[List[int]])
async def get_confusion_matrix(id:str):
    task = await task_data_service.get_active_data_balancing_task_by_related_id(id)
    if task is None:
        model_configuration = await model_configuration_service.get_by_id(id)
        return model_configuration.data_balancing.confusion_matrix

@router.put(END_POINT, tags=[TAG], response_model=ModelConfigurationModel)
async def update_model(model_configuration:ModelConfigurationModel):
    return await model_configuration_service.save(model_configuration)

@router.put(END_POINT + "{id}/databalance", tags=[TAG], response_model=ModelConfigurationModel)
async def update_data_balance(id:str, data_balancing_dto:DataBalancingDto):
    model_configuration = await model_configuration_service.get_by_id(id)
    model_configuration.data_balancing = DataBalancingModel()
    model_configuration.data_balancing.balance_max_delta = data_balancing_dto.balance_max_delta
    model_configuration.data_balancing.cut_off_percentile = data_balancing_dto.cut_off_percentile
    model_configuration.data_balancing.confusion_matrix = data_balancing_dto.confusion_matrix
    await model_configuration_service.save(model_configuration)

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete(id:str):
    return await model_configuration_service.delete_by_id(id)