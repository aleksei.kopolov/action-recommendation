from typing import List
from fastapi import APIRouter
from models.folder_data_model import FolderDataModel
from services.folder_data_service import FolderDataService

folder_data_service = FolderDataService()

router = APIRouter()

END_POINT = "/folderdata/"
TAG = "folderdata"

@router.get(END_POINT, tags=[TAG], response_model=List[FolderDataModel])
async def get_all():
    return await folder_data_service.get_all()

@router.get(END_POINT + "{id}", tags=[TAG], response_model=FolderDataModel)
async def get_file_metadata_by_id(id:str):
    return await folder_data_service.get_by_id(id)

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete_file_metadata(id:str):
    await folder_data_service.delete_by_id(id)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    await folder_data_service.delete_all()