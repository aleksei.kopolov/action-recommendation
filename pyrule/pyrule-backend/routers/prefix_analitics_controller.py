from typing import List
from fastapi import APIRouter

from services.prefix_analitics_service import PrefixAnaliticsService
from models.prefix_analitics_model import PrefixAnaliticsModel
from dtos.prefix_analitics_dto import PrefixAnaliticsDto

router = APIRouter()
prefix_analitics_service = PrefixAnaliticsService()

END_POINT = "/prefixanalitics/"
TAG = "prefixanalitics"


@router.get(END_POINT, tags=[TAG], response_model=List[PrefixAnaliticsModel])
async def get_all():
    return await prefix_analitics_service.get_all()

@router.get(END_POINT + "{model_configuration_id}", tags=[TAG], response_model=PrefixAnaliticsModel)
async def get_by_model_configuration_id(model_configuration_id:str):
    return await prefix_analitics_service.get_by_model_configuration_id(model_configuration_id)

@router.post(END_POINT, tags=[TAG])
async def create(prefix_analitics_dto: PrefixAnaliticsDto):
    return await prefix_analitics_service.save(
            PrefixAnaliticsModel(
                model_configuration_id = prefix_analitics_dto.model_configuration_id,
                case_size_analitics = prefix_analitics_dto.case_size_analitics,
                case_treatment_analitics = prefix_analitics_dto.case_treatment_analitics
            )
        )

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete_by_id(id:str):
    return await prefix_analitics_service.delete_by_id(id)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    return await prefix_analitics_service.delete_all()