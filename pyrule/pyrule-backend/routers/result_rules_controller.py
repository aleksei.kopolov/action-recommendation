from typing import List
from fastapi import APIRouter
from dtos.result_rules_dto import ResultDto
from models.result_rule import ResultRuleModel
from services.result_rule_service import ResultRuleService
from helpers.result_rule_helper import get_result_rules_dto

router = APIRouter()
result_rule_service = ResultRuleService()

END_POINT = "/resultrules/"
TAG = "resultrules"

@router.get(END_POINT, tags=[TAG], response_model=List[ResultRuleModel])
async def get_model_result_rules():
    return await result_rule_service.get_all()

@router.get(END_POINT + "{model_configuration_id}", tags=[TAG], response_model=ResultDto)
async def get_model_result_rules(model_configuration_id:str):
    return await get_result_rules_dto(model_configuration_id)

@router.post(END_POINT, tags=[TAG], response_model=List[ResultRuleModel])
async def create_model(result_rule_dto:ResultDto):
    await result_rule_service.delete_by_model_configuration_id(result_rule_dto.model_configuration_id)
    return await result_rule_service.save_all(result_rules=result_rule_dto.get_model())

@router.delete(END_POINT, tags=[TAG])
async def get_model_result_rules():
    return await result_rule_service.delete_all()