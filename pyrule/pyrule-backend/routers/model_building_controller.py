from typing import List
from fastapi import APIRouter
from dtos.data_balancing_dto import DataBalancingDto
from dtos.model_settings_dto import ModelSettingsDto
from helpers.model_builder_helper import init_model_build
from helpers.data_balancing_helper import init_data_balancing
from models.data_balancing_model import DataBalancingModel
from models.model_settings_model import ModelSettingsModel

from services.model_configuration_service import ModelConfigurationService
from services.project_service import ProjectService
from services.file_data_service import FileDataService

router = APIRouter()

model_configuration_service = ModelConfigurationService()
project_service = ProjectService()
file_data_service = FileDataService()

END_POINT = "/modelbuilding/"
TAG = "modelbuilding"

@router.post(END_POINT + "{id}/balancedata", tags=[TAG])
async def balance_data(id:str, data_balancing: DataBalancingDto):
    model_configuration = await model_configuration_service.get_by_id(id)
    model_configuration.data_balancing = DataBalancingModel()
    model_configuration.data_balancing.cut_off_percentile = data_balancing.cut_off_percentile
    model_configuration.data_balancing.balance_max_delta = data_balancing.balance_max_delta
    await model_configuration_service.save(model_configuration)
    await init_data_balancing(str(model_configuration.id))

@router.post(END_POINT + "{id}/buildmodel", tags=[TAG])
async def build_model(id:str, model_settings: ModelSettingsDto):
    model_configuration = await model_configuration_service.get_by_id(id)
    model_configuration.model_settings = ModelSettingsModel()
    model_configuration.model_settings.evaluationFunction = model_settings.evaluationFunction
    model_configuration.model_settings.max_depth = model_settings.max_depth
    model_configuration.model_settings.min_samples_leaf = model_settings.min_samples_leaf
    model_configuration.model_settings.min_samples_treatment = model_settings.min_samples_treatment
    model_configuration.model_settings.n_reg = model_settings.n_reg
    await model_configuration_service.save(model_configuration);

    project_model = await project_service.get_by_id(model_configuration.project_id)
    original_file_model = await file_data_service.get_by_id(project_model.file_id)
    await init_model_build(str(model_configuration.id), original_file_model)