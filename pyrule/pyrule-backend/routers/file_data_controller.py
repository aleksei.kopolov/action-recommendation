from typing import List
from fastapi import APIRouter
from models.file_data_model import FileDataModel
from services.file_data_service import FileDataService

file_metadata_service = FileDataService()

router = APIRouter()

END_POINT = "/filedata/"
TAG = "filedata"

@router.get(END_POINT, tags=[TAG], response_model=List[FileDataModel])
async def get_all():
    return await file_metadata_service.get_all()

@router.get(END_POINT + "{id}", tags=[TAG], response_model=FileDataModel)
async def get_by_project_id(project_id:str):
    return await file_metadata_service.get_by_project_id(project_id)

@router.post(END_POINT, tags=[TAG], response_model=FileDataModel)
async def create(file_metadata: FileDataModel):
    return await file_metadata_service.save(file_metadata)

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete(id:str):
    await file_metadata_service.delete_by_id(id)