from typing import List
from fastapi import APIRouter
from dtos.task_dto import UpdateTaskMetaDataDto
from models.task_model import TaskModel
from services.task_service import TaskService
from helpers.task_helper import update_task_status

router = APIRouter()

task_data_service = TaskService()

END_POINT = "/task/"
TAG = "task"

@router.get(END_POINT, tags=[TAG], response_model=List[TaskModel])
async def get_all():
    return await task_data_service.get_all()

@router.delete(END_POINT+'{id}', tags=[TAG])
async def delete_by_id(id:str):
    await task_data_service.delete_by_id(id)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    await task_data_service.delete_all()

@router.put(END_POINT, tags=[TAG])
async def update(dto:UpdateTaskMetaDataDto):
    await update_task_status(dto)
