from typing import List
from fastapi import APIRouter
from models.project_model import ProjectModel
from helpers.project_helper import create_project, get_all_projects, get_project_by_id, delete_project_by_id
from dtos.project_dto import CreateProjetcRequestDto, CreateProjectResponseDto

router = APIRouter()

END_POINT = "/project/"
TAG = "project"

@router.get(END_POINT, tags=[TAG], response_model=List[ProjectModel])
async def get_all():
    return await get_all_projects()

@router.get(END_POINT + "{id}", tags=[TAG], response_model=ProjectModel)
async def get_by_id(id:str):
    return await get_project_by_id(id)

@router.post(END_POINT, tags=[TAG], response_model=CreateProjectResponseDto)
async def create(request_dto: CreateProjetcRequestDto):
    return await create_project(request_dto)

@router.delete(END_POINT + "{id}", tags=[TAG])
async def delete(id:str):
    await delete_project_by_id(id)