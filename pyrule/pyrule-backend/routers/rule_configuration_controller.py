from typing import List
from fastapi import APIRouter
from models.rule_configuration_model import RuleConfigurationModel
from services.process_configuration_service import RuleConfigurationService
from dtos.rule_configuration_dto import RuleConfigurationDto

rule_configuration_service = RuleConfigurationService()

router = APIRouter()

END_POINT = "/ruleconfiguration/"
TAG = "ruleconfiguration"

@router.get(END_POINT, tags=[TAG], response_model=List[RuleConfigurationModel])
async def get_all():
    return await rule_configuration_service.get_all()

@router.put(END_POINT, tags=[TAG])
async def update_process(request_dto: RuleConfigurationDto):
    rule_configuration = await rule_configuration_service.get_by_id(request_dto.id)
    if rule_configuration is not None:
        rule_configuration.rules = request_dto.rules
        await rule_configuration_service.save(rule_configuration)

@router.get(END_POINT + "{project_id}", tags=[TAG], response_model=List[RuleConfigurationModel])
async def get_all_by_configuration_id(project_id:str):
    return await rule_configuration_service.get_all_by_project_id(project_id)

@router.delete(END_POINT, tags=[TAG])
async def delete_all():
    all = await rule_configuration_service.get_all()
    for i in all:
        await rule_configuration_service.delete_by_id(i.id)