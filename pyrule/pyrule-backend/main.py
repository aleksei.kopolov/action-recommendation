from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import prefix_data_controller, project_controller, folder_data_controller, project_info_controller, task_controller
from routers import prefix_analitics_controller, rule_configuration_controller
from routers import file_upload_controller, file_data_controller, column_configuration_controller
from routers import result_rules_controller
from routers import model_building_controller, model_configuration_controller
app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins = origins,
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"],
)

app.include_router(project_controller.router)
app.include_router(folder_data_controller.router)
app.include_router(project_info_controller.router)
app.include_router(file_upload_controller.router)
app.include_router(file_data_controller.router)
app.include_router(column_configuration_controller.router)
app.include_router(task_controller.router)
app.include_router(rule_configuration_controller.router)
app.include_router(prefix_data_controller.router)
app.include_router(prefix_analitics_controller.router)
app.include_router(model_building_controller.router)
app.include_router(model_configuration_controller.router)
app.include_router(result_rules_controller.router)
