from typing import List, Optional
from pydantic import BaseModel
from models.rule_model import Operator, Rule
from models.rule_configuration_model import RuleType, RuleConfigurationModel

class RuleDto(BaseModel):
    operator : Optional[Operator]
    value : Optional[str]
    field : Optional[str]

    def set_data(self, rule:Rule):
        self.operator = rule.operator
        self.value = rule.value
        self.field = rule.field

class RuleConfigurationDto(BaseModel):
    id : Optional[str]
    rule_type : Optional[RuleType]
    rules : List[List[RuleDto]] = []

    def set_process_data(self, data: RuleConfigurationModel):
        self.id = str(data.id)
        self.rule_type = data.rule_type
        if data.rules is not None:
            self.rules = data.rules