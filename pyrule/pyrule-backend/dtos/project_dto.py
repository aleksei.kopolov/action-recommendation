from pydantic import BaseModel

class CreateProjetcRequestDto(BaseModel):
    name: str

class CreateProjectResponseDto(BaseModel):
    id: str