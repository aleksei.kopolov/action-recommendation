from models.file_data_model import FileDataModel, FileStatus
from models.task_model import TaskModel

from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel
from dtos.column_configuration_dto import ColumnConfigurationDto
from dtos.pick_list_dto import PickListDto

class ProjectInfoDto(BaseModel):
    project_name : str
    has_file : bool = False

    file_status : Optional[FileStatus]
    file_name : Optional[str]
    file_size : Optional[float]
    file_upload_date : Optional[datetime]

    is_uploading: bool = False
    uploading_percentage: float = 0.0

    has_columns: bool = False

    def set_file_data(self, file_data: FileDataModel):
        self.has_file = True
        self.file_status = file_data.file_status
        self.file_name = file_data.file_name
        self.file_size = file_data.file_size
        self.file_upload_date = file_data.sync_date

    def set_task_data(self, task_data: TaskModel):
        self.is_uploading = True
        self.uploading_percentage = task_data.task_process_percentage


class ProjectColumnsInfoDto(BaseModel):
    columns : List[ColumnConfigurationDto] = []
    column_types : List[PickListDto] = []
    column_data_types : List[PickListDto] = []
