from typing import Optional, List
from datetime import datetime
from pydantic import BaseModel

from models.model_configuration_model import ModelConfigurationModel, ModelStatus

class ProjectModelDto(BaseModel):
    id : Optional[str]
    name : Optional[str]
    create_date : Optional[datetime]
    status : Optional[ModelStatus]

    def set_model_data(self, model:ModelConfigurationModel):
        self.id = str(model.id)
        self.name = model.name
        self.status = model.status
        self.create_date = model.create_date