from pydantic import BaseModel
from datetime import datetime
from typing import List, Optional
from dtos.column_configuration_dto import ColumnConfigurationDto
from dtos.data_balancing_dto import DataBalancingDto
from dtos.model_settings_dto import ModelSettingsDto
from dtos.process_status_dto import ProcessStatusDto
from dtos.rule_configuration_dto import RuleDto


from models.model_configuration_model import ModelConfigurationModel

class ModelConfigurationDto(BaseModel):
    id : Optional[str]
    name : Optional[str]
    project_id : Optional[str]
    create_date : Optional[datetime]

    process : Optional[ProcessStatusDto]

    column_configurations : List[ColumnConfigurationDto] = []
    treatment_rules : List[List[RuleDto]] = []
    result_rules : List[List[RuleDto]] = []

    data_balancing : Optional[DataBalancingDto]
    model_settings : Optional[ModelSettingsDto]

    def set_data(self, model_configuration:ModelConfigurationModel):
        self.process = ProcessStatusDto()
        self.data_balancing = DataBalancingDto()
        self.model_settings = ModelSettingsDto()

        self.id = str(model_configuration.id)
        self.name = model_configuration.name
        self.project_id = model_configuration.project_id
        self.create_date = model_configuration.create_date
        
        for column in model_configuration.column_configurations:
            column_configuration_dto = ColumnConfigurationDto()
            column_configuration_dto.set_data(column)
            self.column_configurations.append(column_configuration_dto)

        for treatment_rule_set in model_configuration.treatment_rules:
            rule_set = []
            for treatment_rule in treatment_rule_set:
                rule = RuleDto()
                rule.set_data(treatment_rule)
                rule_set.append(rule)
            self.treatment_rules.append(rule_set)

        for result_rule_set in model_configuration.result_rules:
            rule_set = []
            for result_rule in result_rule_set:
                rule = RuleDto()
                rule.set_data(result_rule)
                rule_set.append(rule)
            self.result_rules.append(rule_set)
        
        self.data_balancing.set_data(model_configuration.data_balancing)
        self.model_settings.set_data(model_configuration.model_settings)