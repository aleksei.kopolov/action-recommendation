from pydantic import BaseModel

class PickListDto(BaseModel):
    value : str
    label : str