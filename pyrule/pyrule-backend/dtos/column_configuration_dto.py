from typing import Optional
from pydantic import BaseModel
from models.column_configuration_model import ColumnConfigurationModel, ColumnDataType, ColumnType

class ColumnConfigurationDto(BaseModel):
    id : Optional[str]
    project_id : Optional[str]
    column_name : Optional[str]
    column_type : Optional[ColumnType]
    column_data_type : Optional[ColumnDataType]

    def set_data(self, column_configuration:ColumnConfigurationModel):
        self.id = str(column_configuration.id)
        self.project_id = column_configuration.project_id
        self.column_name = column_configuration.column_name
        self.column_type = column_configuration.column_type
        self.column_data_type = column_configuration.column_data_type