from pydantic import BaseModel



class ProcessStatusDto(BaseModel):
    is_processing : bool = False
    process_percentage : float = 0.0
    status : str = ''