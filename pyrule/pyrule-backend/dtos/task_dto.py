from models.task_model import TaskStatus, TaskType
from typing import Optional
from pydantic import BaseModel

class UpdateTaskMetaDataDto(BaseModel):
    id: str
    task_status: Optional[TaskStatus]
    task_process_percentage: Optional[float]
