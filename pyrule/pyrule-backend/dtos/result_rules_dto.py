from typing import List, Optional
from pydantic import BaseModel
from dtos.rule_configuration_dto import RuleDto
from models.result_rule import ResultRuleModel
from models.rule_model import Rule

class ResultRuleDto(BaseModel):
    model_configuration_id: Optional[str]
    rules: List[RuleDto] = []
    uplift_score: Optional[float]

    def get_model(self) -> ResultRuleModel:
        rule_list = []

        for rule in self.rules:
            rule_list.append(Rule(operator=rule.operator, value=rule.value, field=rule.field))

        return ResultRuleModel(
            model_configuration_id=self.model_configuration_id,
            uplift_score=self.uplift_score,
            rules = rule_list
        )
    
    def set_data(self, result_rule:ResultRuleModel):
        self.model_configuration_id = result_rule.model_configuration_id
        self.uplift_score = result_rule.uplift_score
        for rule in result_rule.rules:
            rule_dto = RuleDto()
            rule_dto.set_data(rule)
            self.rules.append(rule_dto)

class ResultDto(BaseModel):
    model_configuration_id: Optional[str]
    is_loading: Optional[bool] = False
    result_rules: List[ResultRuleDto] = []

    def get_model(self) -> List[ResultRuleModel]:
        result_rule_list = []
        for result_rule in self.result_rules:
            result_rule_list.append(result_rule.get_model())
        return result_rule_list

    def set_data(self, result_rules:List[ResultRuleModel]):
        for result_rule in result_rules:
            result_rule_dto = ResultRuleDto()
            result_rule_dto.set_data(result_rule)
            self.result_rules.append(result_rule_dto)
