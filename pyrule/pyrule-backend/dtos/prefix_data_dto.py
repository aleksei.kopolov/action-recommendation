from typing import List, Optional
from pydantic import BaseModel

class PrefixRowDto(BaseModel):
    group_name : Optional[str]
    is_successful : Optional[bool]
    has_treatment : Optional[bool]
    case_start_index : Optional[int]
    case_end_index : Optional[int]
    case_size : Optional[int]
    case_cutoff_index : Optional[int]
    case_prefix_size : Optional[int]

class PrefixDataDto(BaseModel):
    id : Optional[str]
    model_configuration_id : Optional[str]
    prefix_list : List[PrefixRowDto] = []