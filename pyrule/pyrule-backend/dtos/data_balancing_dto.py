from typing import List, Optional
from pydantic import BaseModel

from models.data_balancing_model import DataBalancingModel

class DataBalancingDto(BaseModel):
    cut_off_percentile : Optional[float]
    balance_max_delta : Optional[float]
    confusion_matrix : List[List[int]] = [[]]

    def set_data(self, data_balancing:DataBalancingModel):
        self.cut_off_percentile = data_balancing.cut_off_percentile
        self.balance_max_delta = data_balancing.balance_max_delta
        self.confusion_matrix = data_balancing.confusion_matrix