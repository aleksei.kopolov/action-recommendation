from typing import Optional
from pydantic import BaseModel

from models.model_settings_model import EvaluationFunction, ModelSettingsModel

class ModelSettingsDto(BaseModel):
    evaluationFunction : Optional[EvaluationFunction]
    max_depth : Optional[int]
    min_samples_leaf : Optional[int]
    min_samples_treatment : Optional[int]
    n_reg : Optional[int]
    
    def set_data(self, model_settings:ModelSettingsModel):
        self.evaluationFunction = model_settings.evaluationFunction
        self.max_depth = model_settings.max_depth
        self.min_samples_leaf = model_settings.min_samples_leaf
        self.min_samples_treatment = model_settings.min_samples_treatment
        self.n_reg = model_settings.n_reg