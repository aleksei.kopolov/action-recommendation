from typing import List, Optional
from pydantic import BaseModel

class PrefixAnaliticsRowDto(BaseModel):
    index : Optional[int]
    has_treatment : Optional[int]
    missing_treatment : Optional[int]
    success : Optional[int]
    failed : Optional[int]

class PrefixAnaliticsDto(BaseModel):
    id : Optional[str]
    model_configuration_id : Optional[str]
    case_size_analitics : List[PrefixAnaliticsRowDto] = []
    case_treatment_analitics : List[PrefixAnaliticsRowDto] = []