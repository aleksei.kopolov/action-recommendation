from typing import Optional
from pydantic import BaseModel

class FileContentDataDto(BaseModel):
    name : Optional[str]
    value : Optional[str]