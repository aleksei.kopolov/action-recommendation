from typing import Dict
from pydantic import BaseModel
from models.column_configuration_model import ColumnDataType
from models.rule_model import Operator


class RuleOperatorsDto(BaseModel):
    rule_operators : Dict = {
        ColumnDataType.CATEGORICAL.value : [Operator.EQUAL.name, Operator.NOT_EQUAL.name],
        ColumnDataType.TEXT.value : [Operator.EQUAL.name, Operator.NOT_EQUAL.name],
        ColumnDataType.NUMBER.value : [
            Operator.EQUAL.name, Operator.NOT_EQUAL.name, 
            Operator.LARGER.name, Operator.SMALLER.name, 
            Operator.LARGER_EQUAL.name, Operator.SMALLER_EQUAL.name
        ],
        ColumnDataType.BOOLEAN.value : [Operator.EQUAL.name, Operator.NOT_EQUAL.name],
        ColumnDataType.DATE.value : [
            Operator.EQUAL.name, Operator.NOT_EQUAL.name, 
            Operator.LARGER.name, Operator.SMALLER.name, 
            Operator.LARGER_EQUAL.name, Operator.SMALLER_EQUAL.name
        ]
    }