from datetime import datetime
from typing import Optional
from pydantic import BaseModel
from models.file_data_model import FileStatus

class DropboxUploadResultDto(BaseModel):
    dropbox_id : Optional[str]
    sync_date : Optional[datetime]
    file_size : Optional[float]
    file_status : Optional[FileStatus]