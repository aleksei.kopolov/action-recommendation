import os
from dotenv import load_dotenv

from motor.motor_asyncio import AsyncIOMotorClient
from odmantic import AIOEngine

class MongoService():
    client = None
    engine = None

    def __init__(self):
        load_dotenv()
        self.client = AsyncIOMotorClient(os.getenv('MONGO_DB_ULR'))
        self.engine = AIOEngine(motor_client=self.client, database="main_db")

    