from typing import List
from models.result_rule import ResultRuleModel
from services.mongo_service import MongoService
from odmantic import ObjectId

class ResultRuleService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(ResultRuleModel)

    async def save(self, result_rule:ResultRuleModel) -> ResultRuleModel:
        return await self.engine.save(result_rule)

    async def save_all(self, result_rules:List[ResultRuleModel]) -> List[ResultRuleModel]:
        return await self.engine.save_all(result_rules)

    async def get_by_id(self, id:str) -> ResultRuleModel:
        return await self.engine.find_one(ResultRuleModel, ResultRuleModel.id.eq(ObjectId(id)))

    async def get_by_model_configuration_id(self, model_configuration_id:str) -> ResultRuleModel:
        return await self.engine.find(ResultRuleModel, ResultRuleModel.model_configuration_id.eq(model_configuration_id))

    async def delete_by_id(self, id:str):
        result_rules = await self.get_by_id(id)

        if result_rules is not None:
            await self.engine.delete(result_rules)

    async def delete_by_model_configuration_id(self, model_configuration_id:str):
        result_rules = await self.get_by_model_configuration_id(model_configuration_id)

        for result_rule in result_rules:
            await self.engine.delete(result_rule)

    async def delete_all(self):
        result_rules = await self.get_all()

        for result_rule in result_rules:
            await self.engine.delete(result_rule)