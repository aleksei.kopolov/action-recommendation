from services.mongo_service import MongoService
from models.column_configuration_model import ColumnConfigurationModel
from odmantic import ObjectId
from typing import List

class ColumnConfigurationService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(ColumnConfigurationModel)

    async def get_by_id(self, id:str) -> ColumnConfigurationModel:
        return await self.engine.find_one(ColumnConfigurationModel, ColumnConfigurationModel.id.eq(ObjectId(id)))

    async def get_all_by_project_id(self, project_id:str):
        return await self.engine.find(ColumnConfigurationModel, ColumnConfigurationModel.project_id.eq(project_id))

    async def save(self, columnMetadata:ColumnConfigurationModel):
        return await self.engine.save(columnMetadata)

    async def save_all(self, columnMetadatas:List[ColumnConfigurationModel]):
        return await self.engine.save_all(columnMetadatas)

    async def delete_all(self):
        for column_configuration in await self.get_all():
            await self.engine.delete(column_configuration)

    async def delete_by_id(self, id:str):
        file_metadata = await self.get_by_id(id)

        if file_metadata is not None:
            await self.engine.delete(file_metadata)

    async def delete_all_by_project_id(self, project_id:str):
        column_configurations = await self.get_all_by_project_id(project_id)

        if column_configurations is not None:
            for column_configuration in column_configurations:
                await self.engine.delete(column_configuration)