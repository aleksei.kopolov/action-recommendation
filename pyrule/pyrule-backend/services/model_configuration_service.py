from typing import List
from services.mongo_service import MongoService
from models.model_configuration_model import ModelConfigurationModel
from odmantic import ObjectId

class ModelConfigurationService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(ModelConfigurationModel)

    async def get_by_id(self, id:str) -> ModelConfigurationModel:
        return await self.engine.find_one(ModelConfigurationModel, ModelConfigurationModel.id.eq(ObjectId(id)))

    async def get_by_project_id(self, project_id:str) -> List[ModelConfigurationModel]:
        return await self.engine.find(ModelConfigurationModel, ModelConfigurationModel.project_id.eq(project_id))

    async def save(self, model_configuration:ModelConfigurationModel):
        return await self.engine.save(model_configuration)

    async def delete_by_id(self, id:str):
        model_configuration = await self.get_by_id(id)

        if model_configuration is not None:
            await self.engine.delete(model_configuration)

    async def delete_all(self):
        all_records = await self.engine.find(ModelConfigurationModel)
        for record in all_records:
            await self.engine.delete(record)