from services.mongo_service import MongoService
from models.prefix_data_model import PrefixDataModel
from odmantic import ObjectId

class PrefixDataService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(PrefixDataModel)

    async def get_by_id(self, id:str) -> PrefixDataModel:
        return await self.engine.find_one(PrefixDataModel, PrefixDataModel.id.eq(ObjectId(id)))

    async def get_by_model_configuration_id(self, model_configuration_id:str) -> PrefixDataModel:
        return await self.engine.find_one(PrefixDataModel, PrefixDataModel.model_configuration_id.eq(model_configuration_id))

    async def save(self, prefix_data_model:PrefixDataModel):
        return await self.engine.save(prefix_data_model)

    async def delete_by_id(self, id:str):
        prefix_data_model = await self.get_by_id(id)

        if prefix_data_model is not None:
            await self.engine.delete(prefix_data_model)

    async def delete_all(self):
        all_records = await self.engine.find(PrefixDataModel)
        for record in all_records:
            await self.engine.delete(record)

    async def delete_by_model_configuration_id(self, model_configuration_id:str):
        model_configurations = await self.engine.find(PrefixDataModel, PrefixDataModel.model_configuration_id.eq(model_configuration_id))
        if model_configurations is not None:
            for record in model_configurations:
                await self.engine.delete(record)