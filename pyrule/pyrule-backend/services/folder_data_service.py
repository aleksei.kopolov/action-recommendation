from services.mongo_service import MongoService
from models.folder_data_model import FolderDataModel
from odmantic import ObjectId

class FolderDataService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(FolderDataModel)

    async def get_by_id(self, id:str) -> FolderDataModel:
        return await self.engine.find_one(FolderDataModel, FolderDataModel.id.eq(ObjectId(id)))

    async def save(self, file_metadata:FolderDataModel):
        return await self.engine.save(file_metadata)

    async def delete_by_id(self, id:str):
        project_info = await self.get_by_id(id)

        if project_info is not None:
            await self.engine.delete(project_info)

    async def delete_all(self):
        all_records = await self.engine.find(FolderDataModel)
        for record in all_records:
            await self.engine.delete(record)