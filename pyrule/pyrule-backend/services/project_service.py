from services.mongo_service import MongoService
from models.project_model import ProjectModel
from odmantic import ObjectId

class ProjectService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(ProjectModel)

    async def save(self, project:ProjectModel) -> ProjectModel:
        return await self.engine.save(project)

    async def get_by_id(self, id:str) -> ProjectModel:
        return await self.engine.find_one(ProjectModel, ProjectModel.id.eq(ObjectId(id)))

    async def delete_by_id(self, id:str):
        project = await self.get_by_id(id)

        if project is not None:
            await self.engine.delete(project)