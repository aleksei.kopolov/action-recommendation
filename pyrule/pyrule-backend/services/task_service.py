from services.mongo_service import MongoService
from models.task_model import TaskModel, TaskStatus, TaskType
from odmantic import ObjectId
from typing import List


class TaskService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(TaskModel)

    async def get_by_related_id(self, related_id:str) -> List[TaskModel]:
        return await self.engine.find(TaskModel, TaskModel.related_id.eq(related_id))

    async def get_active_task_by_related_id(self, related_id:str) -> List[TaskModel]:
        return await self.engine.find_one(TaskModel, TaskModel.task_status.eq(TaskStatus.PROCESSING) & TaskModel.related_id.eq(related_id))

    async def get_active_data_processing_task_by_related_id(self, related_id:str) -> List[TaskModel]:
        return await self.engine.find_one(TaskModel, TaskModel.task_status.eq(TaskStatus.PROCESSING) & TaskModel.task_type.eq(TaskType.DATA_PORCESSING) & TaskModel.related_id.eq(related_id))

    async def get_active_model_building_task_by_related_id(self, related_id:str) -> List[TaskModel]:
        return await self.engine.find_one(TaskModel, TaskModel.task_status.eq(TaskStatus.PROCESSING) & TaskModel.task_type.eq(TaskType.MODEL_BUILDING) & TaskModel.related_id.eq(related_id))

    async def get_active_data_balancing_task_by_related_id(self, related_id:str) -> List[TaskModel]:
        return await self.engine.find_one(TaskModel, TaskModel.task_status.eq(TaskStatus.PROCESSING) & TaskModel.task_type.eq(TaskType.DATA_BALANCING) & TaskModel.related_id.eq(related_id))

    async def get_by_id(self, id:str) -> TaskModel:
        return await self.engine.find_one(TaskModel, TaskModel.id.eq(ObjectId(id)))

    async def save(self, taskMetadata:TaskModel):
        return await self.engine.save(taskMetadata)

    async def delete_by_id(self, id:str):
        task_metadata = await self.get_by_id(id)

        if task_metadata is not None:
            await self.engine.delete(task_metadata)

    async def delete_all(self):
        tasks = await self.get_all()
        for task in tasks:
            await self.engine.delete(task)

    async def delete_all_by_related_id(self, related_id:str):
        tasks = await self.engine.find(TaskModel, TaskModel.related_id.eq(related_id))
        if tasks is not None:
            for task in tasks:
                await self.engine.delete(task)