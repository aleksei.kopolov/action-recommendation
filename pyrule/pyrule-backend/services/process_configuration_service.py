from services.mongo_service import MongoService
from models.rule_configuration_model import RuleConfigurationModel, RuleType
from odmantic import ObjectId
from typing import List

class RuleConfigurationService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(RuleConfigurationModel)

    async def get_by_id(self, id:str) -> RuleConfigurationModel:
        return await self.engine.find_one(RuleConfigurationModel, RuleConfigurationModel.id.eq(ObjectId(id)))

    async def get_all_by_project_id(self, project_id:str):
        return await self.engine.find(RuleConfigurationModel, RuleConfigurationModel.project_id.eq(project_id))

    async def get_all_by_project_id_and_rule_type(self, project_id:str, rule_type:RuleType):
        return await self.engine.find_one(RuleConfigurationModel, RuleConfigurationModel.rule_type.eq(rule_type) & RuleConfigurationModel.project_id.eq(project_id))

    async def get_all_treatment_rules_by_project_id(self, project_id:str):
        return await self.engine.find_one(RuleConfigurationModel, RuleConfigurationModel.rule_type.eq(RuleType.TREATMENT) & RuleConfigurationModel.project_id.eq(project_id))

    async def get_all_result_rules_by_project_id(self, project_id:str):
        return await self.engine.find_one(RuleConfigurationModel, RuleConfigurationModel.rule_type.eq(RuleType.RESULT) & RuleConfigurationModel.project_id.eq(project_id))

    async def save(self, process_configuration:RuleConfigurationModel):
        return await self.engine.save(process_configuration)

    async def save_all(self, process_configuration:List[RuleConfigurationModel]):
        return await self.engine.save_all(process_configuration)

    async def delete_all_by_project_id(self, project_id:str):
        data_configuration = await self.get_all_by_project_id(project_id)

        if data_configuration is not None:
            await self.engine.delete(data_configuration)

    async def delete_by_id(self, id:str):
        process_configuration = await self.get_by_id(id)

        if process_configuration is not None:
            await self.engine.delete(process_configuration)