import os, dropbox
from dotenv import load_dotenv

CHUNK_SIZE = 5 * 1024 * 1024

class DropBoxService():
    client = None

    def __init__(self):
        load_dotenv()
        self.client = dropbox.Dropbox(os.getenv('DROP_BOX_TOKEN'))

    def get_folder_metadata(self, folder_name:str = ''):
        return self.client.files_list_folder(folder_name).entries

    def create_folder(self, folder_name:str):
        return self.client.files_create_folder_v2(folder_name)

    def upload_small_file(self, file, dropbox_file_path:str):
        return self.client.files_upload(file.read(), dropbox_file_path)

    def start_large_file_upload_session(self, file):
        return self.client.files_upload_session_start(file.read(CHUNK_SIZE))

    def upload_file_chunk(self, file, cursor):
        self.client.files_upload_session_append_v2(file.read(CHUNK_SIZE), cursor)

    def end_large_file_upload(self, file, cursor, commit):
        return self.client.files_upload_session_finish(file.read(CHUNK_SIZE), cursor, commit)

    def get_file_metadata(self, file_path):
        return self.client.files_get_metadata(file_path)

    def download_file(self, local_path:str, file_dropbox_id:str):
        return self.client.files_download_to_file(download_path=local_path, path=file_dropbox_id)

    def delet_folder_by_dropbox_id(self, folder_id:str):
        try :
            self.client.files_delete_v2(folder_id)
        except:
            pass