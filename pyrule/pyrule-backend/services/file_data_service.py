from services.mongo_service import MongoService
from models.file_data_model import FileDataModel, FileType
from odmantic import ObjectId
from typing import List


class FileDataService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(FileDataModel)

    async def get_by_id(self, id:str) -> FileDataModel:
        return await self.engine.find_one(FileDataModel, FileDataModel.id.eq(ObjectId(id)))

    async def save(self, file_data:FileDataModel):
        return await self.engine.save(file_data)

    async def delete_by_id(self, id:str):
        file_data = await self.get_by_id(id)

        if file_data is not None:
            await self.engine.delete(file_data)