from services.mongo_service import MongoService
from models.prefix_analitics_model import PrefixAnaliticsModel
from odmantic import ObjectId

class PrefixAnaliticsService(MongoService):
    def __init__(self):
        super().__init__()

    async def get_all(self):
        return await self.engine.find(PrefixAnaliticsModel)

    async def get_by_id(self, id:str) -> PrefixAnaliticsModel:
        return await self.engine.find_one(PrefixAnaliticsModel, PrefixAnaliticsModel.id.eq(ObjectId(id)))

    async def get_by_model_configuration_id(self, model_configuration_id:str) -> PrefixAnaliticsModel:
        return await self.engine.find_one(PrefixAnaliticsModel, PrefixAnaliticsModel.model_configuration_id.eq(model_configuration_id))

    async def save(self, prefix_data_model:PrefixAnaliticsModel):
        return await self.engine.save(prefix_data_model)

    async def delete_by_id(self, id:str):
        prefix_data_model = await self.get_by_id(id)

        if prefix_data_model is not None:
            await self.engine.delete(prefix_data_model)

    async def delete_all(self):
        all_records = await self.engine.find(PrefixAnaliticsModel)
        for record in all_records:
            await self.engine.delete(record)

    async def delete_by_model_configuration_id(self, model_configuration_id:str):
        prefix_analitics =  await self.engine.find(PrefixAnaliticsModel, PrefixAnaliticsModel.model_configuration_id.eq(model_configuration_id))
        if prefix_analitics is not None:
            for record in prefix_analitics:
                await self.engine.delete(record)