import uuid
from services.dropbox_service import DropBoxService

dropbox_service = DropBoxService()

def creat_folder_for_project():
    return create_folder()

def create_folder(path:str=''):
    return dropbox_service.create_folder(path + '/' + str(uuid.uuid4()))

def get_folder_metadata(dropbox_id:str):
    return dropbox_service.get_file_metadata(dropbox_id)

def delete_folder_by_id(dropbox_id:str):
    dropbox_service.delet_folder_by_dropbox_id(dropbox_id)