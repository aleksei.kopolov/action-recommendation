from services.project_service import ProjectService
from services.folder_data_service import FolderDataService
from services.file_data_service import FileDataService
from services.column_configuration_service import ColumnConfigurationService
from services.model_configuration_service import ModelConfigurationService
from services.result_rule_service import ResultRuleService
from services.prefix_data_service import PrefixDataService
from services.prefix_analitics_service import PrefixAnaliticsService
from services.task_service import TaskService

from datetime import datetime
from models.project_model import ProjectModel
from models.folder_data_model import FolderDataModel
from dtos.project_dto import CreateProjetcRequestDto, CreateProjectResponseDto
from helpers.folder_helper import creat_folder_for_project, delete_folder_by_id

project_service = ProjectService()
folder_data_service = FolderDataService()
file_data_service = FileDataService()
column_configuration_service = ColumnConfigurationService()
model_configuration_service = ModelConfigurationService()
result_rules_service = ResultRuleService()
prefix_data_service = PrefixDataService()
prefix_analitics_service = PrefixAnaliticsService()
task_service = TaskService()

async def get_all_projects():
    return await project_service.get_all()

async def get_project_by_id(id:str):
    return await project_service.get_by_id(id)

async def create_project(request_dto:CreateProjetcRequestDto):
    folder_create_result = creat_folder_for_project()
    folder_data = await folder_data_service.save(FolderDataModel(dropbox_id = folder_create_result.metadata.id, sync_date = datetime.now()))
    project_result = await project_service.save(ProjectModel(name = request_dto.name, folder_id = str(folder_data.id), sync_date = datetime.now()))
    return CreateProjectResponseDto(id=str(project_result.id))

async def delete_project_by_id(project_id:str):
    project = await project_service.get_by_id(project_id)

    file_metadata = await folder_data_service.get_by_id(project.folder_id)
    if file_metadata is not None:
        delete_folder_by_id(file_metadata.dropbox_id)
        await folder_data_service.engine.delete(file_metadata)

    await file_data_service.delete_by_id(project.file_id)
    await project_service.delete_by_id(project_id)
    await column_configuration_service.delete_all_by_project_id(project_id)
    await task_service.delete_all_by_related_id(project_id)

    model_configurations = await model_configuration_service.get_by_project_id(project_id)
    if model_configurations is not None:
        for model_configuration in model_configurations:
            await result_rules_service.delete_by_model_configuration_id(str(model_configuration.id))
            await prefix_data_service.delete_by_model_configuration_id(str(model_configuration.id))
            await prefix_analitics_service.delete_by_model_configuration_id(str(model_configuration.id))
            await task_service.delete_all_by_related_id(str(model_configuration.id))

    