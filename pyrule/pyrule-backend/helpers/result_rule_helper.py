
from dtos.result_rules_dto import ResultDto
from services.result_rule_service import ResultRuleService
from services.task_service import TaskService

result_rule_service = ResultRuleService()
task_data_service = TaskService()

async def get_result_rules_dto(model_configuration_id:str):
    task = await task_data_service.get_active_model_building_task_by_related_id(model_configuration_id)
    result_rules = await result_rule_service.get_by_model_configuration_id(model_configuration_id)
    result_dto = ResultDto()
    result_dto.set_data(result_rules)
    if task is not None:
        result_dto.is_loading = True
    return result_dto