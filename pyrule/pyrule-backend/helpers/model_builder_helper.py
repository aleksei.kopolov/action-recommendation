import uuid

from models.file_data_model import FileDataModel

from helpers.file_helper import TEMP_FOLDER
from helpers.task_helper import create_main_file_download_task_metadata, create_model_build_task_metadata

from celery import chain
from worker.app import download_main_file, build_model, delete_temp_file

async def init_model_build(model_configuration_id:str, file_model:FileDataModel):
    download_task = await create_main_file_download_task_metadata(str(model_configuration_id))
    process_task = await create_model_build_task_metadata(str(model_configuration_id))

    file_local_path = TEMP_FOLDER + str(uuid.uuid4()) + '.csv.gz'

    chain(
        download_main_file.si(file_local_path, file_model.dropbox_id, str(download_task.id)),
        build_model.si(file_local_path, model_configuration_id, str(process_task.id)),
        delete_temp_file.si(file_local_path)
    )()