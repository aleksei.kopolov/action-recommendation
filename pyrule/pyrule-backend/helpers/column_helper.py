from typing import List
from services.column_configuration_service import ColumnConfigurationService
from models.column_configuration_model import ColumnConfigurationModel
from dtos.column_configuration_dto import ColumnConfigurationDto

column_configuration_service = ColumnConfigurationService()

async def create_project_columns(project_id : str, column_names:List[str]):
    new_columns = []

    for column_name in column_names:
        new_columns.append(ColumnConfigurationModel(
            project_id = project_id,
            column_name = column_name
        ))

    await column_configuration_service.save_all(new_columns)

    
async def update_column_configuration(column_configuration : ColumnConfigurationDto):
    column = await column_configuration_service.get_by_id(column_configuration.id);

    if column is not None:
        column.column_type = column_configuration.column_type
        column.column_data_type = column_configuration.column_data_type
        await column_configuration_service.save(column)

def get_dto_from_model(column_models : List[ColumnConfigurationModel]):
    dto = []

    for column in column_models:
        dto.append(convert_column_model_to_dto(column))

    return dto


def convert_column_model_to_dto(column_model : ColumnConfigurationModel):
    return ColumnConfigurationDto(
        id = str(column_model.id),
        column_name = column_model.column_name,
        column_type = column_model.column_type,
        column_data_type = column_model.column_data_type
    )