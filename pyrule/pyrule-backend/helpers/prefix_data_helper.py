from dtos.prefix_data_dto import PrefixDataDto
from models.prefix_data_model import PrefixDataModel
from services.prefix_data_service import PrefixDataService

prefix_data_service = PrefixDataService()

async def save_prefix_data(prefix_data_dto:PrefixDataDto):
    return await prefix_data_service.save(PrefixDataModel(model_configuration_id=prefix_data_dto.model_configuration_id, prefix_list=prefix_data_dto.prefix_list))