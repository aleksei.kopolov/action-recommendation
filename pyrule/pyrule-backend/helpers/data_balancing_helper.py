from helpers.task_helper import create_data_balencing_task_metadata

from celery import chain
from worker.app import balance_data

async def init_data_balancing(model_configuration_id:str):
    process_task = await create_data_balencing_task_metadata(str(model_configuration_id))
    chain(balance_data.si(model_configuration_id, str(process_task.id)))()