from dtos.task_dto import UpdateTaskMetaDataDto
from datetime import datetime
from models.task_model import TaskModel, TaskStatus, TaskType
from services.task_service import TaskService

task_data_service = TaskService()

async def update_task_status(dto:UpdateTaskMetaDataDto):
    if dto is not None and dto.id is not None:
        task = await task_data_service.get_by_id(dto.id)
        if task is not None:

            task.task_status = dto.task_status
            task.task_process_percentage = dto.task_process_percentage

            if dto.task_status == TaskStatus.DONE:
                task.end_date = datetime.now()

            await task_data_service.save(task)


async def create_main_file_upload_task(related_id:str):
    return await task_data_service.save(TaskModel(
        related_id = related_id,
        task_type = TaskType.FILE_UPLOAD,
        task_status = TaskStatus.PROCESSING
    ))


async def create_main_file_download_task_metadata(related_id:str):
    return await task_data_service.save(TaskModel(
        related_id = related_id,
        task_type = TaskType.FILE_DOWNLOAD,
        task_status = TaskStatus.PROCESSING
    ))

async def create_main_file_process_task_metadata(related_id:str):
    return await task_data_service.save(TaskModel(
        related_id = related_id,
        task_type = TaskType.DATA_PORCESSING,
        task_status = TaskStatus.PROCESSING
    ))

async def create_model_build_task_metadata(related_id:str):
    return await task_data_service.save(TaskModel(
        related_id = related_id,
        task_type = TaskType.MODEL_BUILDING,
        task_status = TaskStatus.PROCESSING
    ))

async def create_data_balencing_task_metadata(related_id:str):
    return await task_data_service.save(TaskModel(
        related_id = related_id,
        task_type = TaskType.DATA_BALANCING,
        task_status = TaskStatus.PROCESSING
    ))