from datetime import datetime
from dtos.rule_configuration_dto import RuleConfigurationDto
from dtos.project_info_dto import ProjectInfoDto, ProjectColumnsInfoDto
from dtos.column_configuration_dto import ColumnDataType, ColumnType
from dtos.pick_list_dto import PickListDto
from dtos.project_model_dto import ProjectModelDto
from helpers.data_preprocess_helper import init_data_preprocessing

from models.file_data_model import FileStatus
from models.model_configuration_model import ModelConfigurationModel, ModelStatus
from models.rule_configuration_model import RuleConfigurationModel, RuleType
from services.model_configuration_service import ModelConfigurationService

from services.project_service import ProjectService
from services.file_data_service import FileDataService
from services.task_service import TaskService
from services.column_configuration_service import ColumnConfigurationService
from services.process_configuration_service import RuleConfigurationService

from helpers.column_helper import get_dto_from_model

project_service = ProjectService()
rule_configuration_service = RuleConfigurationService()
column_configuration_service = ColumnConfigurationService()
model_configuration_service = ModelConfigurationService()

file_data_service = FileDataService()
task_data_service = TaskService()

async def get_project_general_info(project_id: str):
    project = await project_service.get_by_id(project_id)
    if project is None: raise Exception("Project not found")

    response_dto = ProjectInfoDto(project_name=project.name)
    file_metadata = await file_data_service.get_by_id(project.file_id)
    column_configurations = await column_configuration_service.get_all_by_project_id(project_id)

    if len(column_configurations) > 0:
        response_dto.has_columns = True
        
    if file_metadata is not None:
        response_dto.set_file_data(file_metadata)

        if file_metadata.file_status == FileStatus.UPLOADING:
            task = await task_data_service.get_active_task_by_related_id(str(file_metadata.id))
            if task is not None:
                response_dto.set_task_data(task)
    
    return response_dto

async def get_project_columns_info(project_id: str):
    configuration = ProjectColumnsInfoDto()
    configuration.columns = get_dto_from_model(await column_configuration_service.get_all_by_project_id(project_id))
    configuration.column_data_types = get_picklist_dto(ColumnDataType)
    configuration.column_types = get_picklist_dto(ColumnType)
    return configuration

def get_picklist_dto(enums):
    picklist_columns = []
    for enum in enums:
        picklist_columns.append(PickListDto(value = enum.value, label = enum.name))
    return picklist_columns
    
async def get_project_treatment_rules(project_id: str, rule_type:RuleType = RuleType.TREATMENT):
    return await get_project_rules(project_id, rule_type)

async def get_project_result_rules(project_id: str, rule_type:RuleType = RuleType.RESULT):
    return await get_project_rules(project_id, rule_type)

async def get_project_rules(project_id: str, rule_type:RuleType):
    rule_model = await rule_configuration_service.get_all_by_project_id_and_rule_type(project_id, rule_type)
    
    if rule_model is None:
        rule_model = await rule_configuration_service.save(RuleConfigurationModel(project_id=project_id, rule_type=rule_type))

    rule_configuration = RuleConfigurationDto()
    rule_configuration.set_process_data(rule_model)
    return rule_configuration;

async def get_project_models(project_id:str):
    models = await model_configuration_service.get_by_project_id(project_id)
    project_model_dtos = []

    for model in models:
        project_model_dto = ProjectModelDto()
        project_model_dto.set_model_data(model)
        project_model_dtos.append(project_model_dto)

    return project_model_dtos

async def create_model_configuration(project_id:str, project_model_dto:ProjectModelDto):
    column_configurations = await column_configuration_service.get_all_by_project_id(project_id)
    treatment_rules = await rule_configuration_service.get_all_treatment_rules_by_project_id(project_id)
    result_rules = await rule_configuration_service.get_all_result_rules_by_project_id(project_id)

    model_configuration = await model_configuration_service.save(ModelConfigurationModel(
        name = project_model_dto.name,
        project_id = project_id,
        create_date = datetime.now(),
        status = ModelStatus.INITIATED,
        column_configurations = column_configurations,
        treatment_rules = treatment_rules.rules,
        result_rules = result_rules.rules
    ))

    project = await project_service.get_by_id(project_id)
    file_model = await file_data_service.get_by_id(project.file_id)
    await init_data_preprocessing(str(model_configuration.id), file_model)
    
    return ProjectModelDto(id=str(model_configuration.id))