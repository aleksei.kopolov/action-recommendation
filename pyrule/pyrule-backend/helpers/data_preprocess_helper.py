import uuid

from models.file_data_model import FileDataModel

from helpers.file_helper import TEMP_FOLDER
from helpers.task_helper import create_main_file_download_task_metadata, create_main_file_process_task_metadata, create_data_balencing_task_metadata

from celery import chain
from worker.app import balance_data, download_main_file, preprocess_file, delete_temp_file

async def init_data_preprocessing(model_configuration_id:str, file_model:FileDataModel):
    download_task = await create_main_file_download_task_metadata(str(model_configuration_id))
    process_task = await create_main_file_process_task_metadata(str(model_configuration_id))
    balancing_task = await create_data_balencing_task_metadata(str(model_configuration_id))

    file_local_path = TEMP_FOLDER + str(uuid.uuid4()) + '.csv.gz'

    chain(
        download_main_file.si(file_local_path, file_model.dropbox_id, str(download_task.id)),
        preprocess_file.si(file_local_path, model_configuration_id, str(process_task.id)),
        delete_temp_file.si(file_local_path),
        balance_data.si(model_configuration_id, str(balancing_task.id))
    )()