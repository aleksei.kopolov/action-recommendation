from fastapi import UploadFile
from models.file_data_model import FileDataModel, FileType, FileStatus
from models.project_model import ProjectModel

from services.file_data_service import FileDataService
from services.task_service import TaskService
from services.folder_data_service import FolderDataService
from services.project_service import ProjectService
from dtos.dropbox_file_dto import DropboxUploadResultDto
from helpers.task_helper import create_main_file_upload_task
from worker.app import upload_main_file, extract_main_file_columns, delete_temp_file, compress_file
from celery import chain
import aiofiles, uuid

file_data_service = FileDataService()
folder_data_service = FolderDataService()
task_data_service = TaskService()
project_service = ProjectService()

TEMP_FOLDER = 'temp/'

async def create_project_file_metadata(file_name: str):
    file_data = await file_data_service.save(FileDataModel(
        file_type = FileType.ORIGINAL,
        file_status = FileStatus.UPLOADING,
        file_name = file_name,
        file_size = 0
    ))
    return file_data

async def update_porject_file_id(project:ProjectModel, file_data:FileDataModel):
    project.file_id = str(file_data.id)
    await project_service.save(project)

async def upload_main_file_to_dropbox(project:ProjectModel, file_data:FileDataModel, upload_file: UploadFile):
    file_path = TEMP_FOLDER + str(uuid.uuid4()) + '.csv'

    async with aiofiles.open(file_path , 'wb') as f:
        content = await upload_file.read()
        await f.write(content)

    folder_metadata = await folder_data_service.get_by_id(project.folder_id)
    task = await create_main_file_upload_task(str(file_data.id))

    chain(
        extract_main_file_columns.s(file_path, str(project.id)),
        compress_file.s(),
        upload_main_file.s(folder_metadata.dropbox_id, str(project.id), str(task.id)),
        delete_temp_file.s()
    )()

async def update_file_data_after_dropbox_upload(project_id:str, upload_result_dto:DropboxUploadResultDto):
    project = await project_service.get_by_id(project_id)
    file_metadata = await file_data_service.get_by_id(project.file_id)

    if upload_result_dto.file_status == FileStatus.UPLOADED:
        file_metadata.file_status = FileStatus.UPLOADED
        file_metadata.file_size = upload_result_dto.file_size
        file_metadata.dropbox_id = upload_result_dto.dropbox_id
        file_metadata.sync_date = upload_result_dto.sync_date
    elif upload_result_dto.file_status == FileStatus.UPLOAD_ERROR:
        file_metadata.file_status = FileStatus.UPLOAD_ERROR

    await file_data_service.save(file_metadata)
