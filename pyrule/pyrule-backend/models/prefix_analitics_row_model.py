from odmantic import Model

class PrefixAnaliticsRowModel(Model):
    index : int
    has_treatment : int
    missing_treatment : int
    success : int
    failed : int