from enum import Enum
from typing import List, Optional
from odmantic import Model
from models.rule_model import Rule

class RuleType(str, Enum):
    RESULT = 'RESULT'
    TREATMENT = 'TREATMENT'

class RuleConfigurationModel(Model):
    project_id : Optional[str]
    rule_type : Optional[RuleType]
    rules : Optional[List[List[Rule]]]