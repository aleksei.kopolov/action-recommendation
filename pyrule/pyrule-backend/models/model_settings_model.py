from odmantic import Model
from enum import Enum

class EvaluationFunction(str, Enum):
    KL = 'KL'

class ModelSettingsModel(Model):
    evaluationFunction : EvaluationFunction = EvaluationFunction.KL
    max_depth : int = 5
    min_samples_leaf : int = 20
    min_samples_treatment : int = 5
    n_reg : int = 10
    