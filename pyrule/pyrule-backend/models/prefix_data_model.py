from typing import List
from odmantic import Model
from models.prefix_row_model import PrefixRowModel

class PrefixDataModel(Model):
    model_configuration_id : str
    prefix_list:List[PrefixRowModel] = []
