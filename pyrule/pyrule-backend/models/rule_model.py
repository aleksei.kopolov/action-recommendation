from enum import Enum
from odmantic import Model

class Operator(str, Enum):
    LARGER = 'LARGER'
    SMALLER = 'SMALLER'
    EQUAL = 'EQUAL'
    NOT_EQUAL = 'NOT_EQUAL'
    LARGER_EQUAL = 'LARGER_EQUAL'
    SMALLER_EQUAL = 'SMALLER_EQUAL'

class Rule(Model):
    operator : Operator
    value : str
    field : str
