from typing import List
from odmantic import Model

class DataBalancingModel(Model):
    cut_off_percentile : float = 0.95
    balance_max_delta : float = 0.15
    confusion_matrix : List[List[int]] = [[]]