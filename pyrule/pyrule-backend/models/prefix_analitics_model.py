from typing import List
from odmantic import Model

from models.prefix_analitics_row_model import PrefixAnaliticsRowModel

class PrefixAnaliticsModel(Model):
    model_configuration_id : str
    case_size_analitics : List[PrefixAnaliticsRowModel] = []
    case_treatment_analitics : List[PrefixAnaliticsRowModel] = []