from enum import Enum
from typing import Optional
from datetime import datetime
from odmantic import Model

class TaskType(str, Enum):
    FILE_UPLOAD = 'FILE_UPLOAD'
    FILE_DOWNLOAD = 'FILE_DOWNLOAD'
    DATA_PORCESSING = 'DATA_PROCESSING'
    DATA_BALANCING = 'DATA_BALANCING'
    MODEL_BUILDING = 'MODEL_BUILDING'

class TaskStatus(str, Enum):
    PROCESSING = 'PROCESSING'
    DONE = 'DONE'
    FAILED = 'FAILED'

class TaskModel(Model):
    related_id : str

    task_type : TaskType
    task_status : TaskStatus

    task_process_percentage : float = 0.0

    start_date : datetime = datetime.now()
    end_date : Optional[datetime]