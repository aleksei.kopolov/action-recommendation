from typing import Optional
from datetime import datetime
from odmantic import Model, Field

class FolderDataModel(Model):
    dropbox_id : str
    parent_folder_id : Optional[str]
    sync_date : Optional[datetime]