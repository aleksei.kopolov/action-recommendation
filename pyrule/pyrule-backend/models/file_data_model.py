from enum import Enum
from typing import Optional, List
from datetime import datetime
from odmantic import Model, Field

class FileType(str, Enum):
    ORIGINAL = 'ORIGINAL'
    PREPROCESSED = 'PREPROCESSED'

class FileStatus(str, Enum):
    UPLOADING = 'UPLOADING'
    UPLOADED = 'UPLOADED'
    UPLOAD_ERROR = 'UPLOAD_ERROR'

class FileDataModel(Model):
    dropbox_id : Optional[str]

    file_type : FileType
    file_status : FileStatus

    file_name : str
    file_size : float
    sync_date : Optional[datetime]