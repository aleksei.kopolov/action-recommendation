from typing import List
from odmantic import Model

class PrefixRowModel(Model):
    group_name : str
    is_successful : bool
    has_treatment : bool
    case_start_index : int
    case_end_index : int
    case_size : int
    case_cutoff_index : int
    case_prefix_size : int