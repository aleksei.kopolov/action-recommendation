from datetime import datetime
from typing import List, Optional
from odmantic import Model
from enum import Enum

from models.column_configuration_model import ColumnConfigurationModel
from models.data_balancing_model import DataBalancingModel
from models.model_settings_model import ModelSettingsModel
from models.rule_model import Rule

class ModelStatus(str, Enum):
    INITIATED = 'INITIATED'
    PREPROCESSING = 'PREPROCESSING'
    PREPROCESSING_READY = 'PREPROCESSING_READY'
    ERROR = 'ERROR'

class ModelConfigurationModel(Model):
    name : str
    project_id : str
    create_date : datetime
    status : Optional[ModelStatus]

    column_configurations : List[ColumnConfigurationModel]
    treatment_rules : List[List[Rule]]
    result_rules : List[List[Rule]]

    data_balancing : Optional[DataBalancingModel] = DataBalancingModel()
    model_settings : Optional[ModelSettingsModel] = ModelSettingsModel()