from enum import Enum
from typing import Optional
from odmantic import Model, Field

class ColumnDataType(str, Enum):
    TEXT = 'TEXT'
    NUMBER = 'NUMBER'
    BOOLEAN = 'BOOLEAN'
    DATE = 'DATE'
    CATEGORICAL = 'CATEGORICAL'

class ColumnType(str, Enum):
    UNCONTROLLABLE = 'UNCONTROLLABLE'
    CONTROLLABLE = 'CONTROLLABLE'
    OUTCOME = 'OUTCOME'
    GROUP_IDENTIFIER = 'GROUP_IDENTIFIER'
    IGNORE = 'IGNORE'
    ORDER_BY = 'ORDER_BY'

class ColumnConfigurationModel(Model):
    project_id : Optional[str] = Field()
    column_name : str = Field()
    column_type : Optional[ColumnType]
    column_data_type : Optional[ColumnDataType]