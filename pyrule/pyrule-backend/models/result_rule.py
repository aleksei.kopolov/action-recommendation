from typing import List, Optional
from odmantic import Model

from models.rule_model import Rule


class ResultRuleModel(Model):
    model_configuration_id : Optional[str]
    rules : List[Rule] = []
    uplift_score: Optional[float]