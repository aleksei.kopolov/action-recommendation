from typing import Optional
from datetime import datetime
from odmantic import Model, Field

class ProjectModel(Model):
    name: str
    
    file_id : Optional[str]
    folder_id : Optional[str]

    sync_date: Optional[datetime]