import Loader from "react-loader-spinner";
import "./spinner.css";

function Spinner(props) {
    return (
        <div className="spinner">
            {(props.show && <Loader type="ThreeDots" color="#2BAD60" height="100" width="100" />)}
        </div>
    );
}
  
export default Spinner;