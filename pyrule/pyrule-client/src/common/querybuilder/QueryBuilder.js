import { Builder } from '@vojtechportes/react-query-builder';
import {useState, useEffect} from 'react';
import './querybuilder.css'

function QueryBuilder(props) {
    const [data, setData] = useState(props.data);
    const [loaded, setLoaded] = useState([]);

    //todo: fix init reqursion
    useEffect(() => {
        if(loaded.length >= 2){
            const timeOutId = setTimeout(() => {props.onQueryUpdate(data)}, 2500);
            return () => clearTimeout(timeOutId);
        }else{
            loaded.push(true);
            return null;
        }
    }, [data])

    return (
        <div className="querybuilder">
            <Builder
                readOnly={false}
                fields={props.columns}
                data={data}
                onChange={data => setData(data)}
            />
        </div>
    );
}

export default QueryBuilder;