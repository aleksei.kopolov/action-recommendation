import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import AppNavBar from './components/nav/AppNavBar';
import Project from './components/projects/Project';
import ProjectInfo from './components/projectinfo/ProjectInfo';
import Model from './components/model/Model';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
    const endpoint = process.env.REACT_APP_BACK_END_URL;

    return (
        <Router>
            <div className="App">
                <AppNavBar/>
                <div className="content">
                    <Switch>
                        <Route exact path="/">
                            <Project endpoint = {endpoint}/>
                        </Route>
                        <Route path="/project/:id">
                            <ProjectInfo endpoint = {endpoint}/>
                        </Route>
                        <Route path="/modelconfiguration/:id">
                            <Model endpoint = {endpoint}/>
                        </Route>
                    </Switch>
                </div>
            </div>
        </Router>
    );
}

export default App;
