import {useState} from 'react';
import ProjectTable from './ProjectTable'
import CreateProject from './CreateProject'
import {Row, Col, Container, Card} from 'react-bootstrap';

function Project(props) {
    const [showModal, setShowModal] = useState(false);

    const endpoint = props.endpoint + 'project/'

    return (
        <div className="project">
            <Container fluid={true}>
                <Row className='mt-4'>
                    <Col className='ml-1'>
                        <Card>
                            <Card.Body>
                                ProLift allows you to discover rules to improve the performance of your business process.<br/>
                                ProLift discovers rules of the form: Precondition, Treatment → Outcome, which increase the success rate of a business process by triggering an action (treatment) when a condition holds.<br/>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col className='ml-1'>
                        <Card>
                            <Card.Body>
                                To start using ProLift, first create a project and give it a name.
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        <ProjectTable 
                            endpoint={endpoint}
                            onShowModle={() => setShowModal(true)}/>
                        <CreateProject 
                            endpoint={endpoint}
                            show={showModal} 
                            onHide={() => setShowModal(false)}/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
  }
  
  export default Project;