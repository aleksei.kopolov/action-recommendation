import {useState} from 'react';
import { useHistory } from 'react-router-dom';
import {Modal, Button, Form} from 'react-bootstrap';
import Spinner from '../../common/spinner/Spinner';

function CreateProject(props){
    const [validated, setValidated] = useState(false);
    const [isPending, setIsPending] = useState(false);
    const [newProjectName, setNewProjectName] = useState('');

    const history = useHistory();

    const handleSubmit = (event) => {
        setValidated(true);
        event.preventDefault();
        event.stopPropagation();

        if (event.currentTarget.checkValidity() === true) {
            setIsPending(true);
            creatNewPorject();
            setValidated(false);
        }
    };

    const creatNewPorject = () =>{
        fetch(props.endpoint, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'accept': 'application/json'
                },
                body: JSON.stringify({name : newProjectName})
            })
            .then(res => {
                if(!res.ok){throw Error('Creation faild');}
                return res.json();
            })
            .then(data =>{
                history.push('/project/'+data.id);
            })
            .catch((err) =>{
                console.log(err);
            });
    }

    return (
        <div className="createproject">
            <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Create New Project
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Spinner show={isPending}/>
                    {(!isPending && 
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group controlId="projectName">
                                <Form.Label>Project Name</Form.Label>
                                <Form.Control
                                    required
                                    autoComplete = 'off'
                                    value={newProjectName}
                                    onChange={e => setNewProjectName(newProjectName => e.target.value)}
                                    type="text"
                                    placeholder="Project Name"/>
                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">Please choose a project name</Form.Control.Feedback>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                        )}
                </Modal.Body>
            </Modal>
        </div>
    )
}

export default CreateProject;