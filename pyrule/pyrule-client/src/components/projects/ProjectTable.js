import BootstrapTable from 'react-bootstrap-table-next';
import {BsTrash, BsFileEarmarkPlus} from 'react-icons/bs';
import {Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import Spinner from '../../common/spinner/Spinner';

function ProjectTable(props) {
    const [projects, setProjects] = useState([]);
    const [isPending, setIsPending] = useState(true);

    const history = useHistory();

    useEffect(() => {
        const abortConstant = new AbortController();

        getAllProjects(abortConstant);

        return () => abortConstant.abort();
    }, []);

    const getAllProjects = (abortConstant) =>{
        fetch(props.endpoint, {signal: abortConstant.signal})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                setProjects(data);
                setIsPending(false);
            })
            .catch((err) =>{
                if(err.name !== 'AbortError'){
                    setProjects([]);
                    setIsPending(false);
                }
            });
    };

    const columns = [
        {
            dataField: 'id',
            text: 'Id',
            hidden: true,
            headerClasses: 'align-middle',
            classes: 'align-middle',
            style: () => {return{cursor: 'pointer'};}
        },
        {
            dataField: 'name',
            text: 'Project Name',
            headerTitle: true,
            headerClasses: 'align-middle',
            classes: 'align-middle',
            style: () => {return{cursor: 'pointer'};}
        },
        {
            dataField: 'sync_date',
            text: 'Upload Date',
            headerTitle: true,
            classes: 'align-middle',
            headerClasses: 'align-middle',
            style: () => {return{cursor: 'pointer'};},
            formatter: (cell) => {
                let dateObj = cell;
                if (typeof cell !== 'object') {dateObj = new Date(cell);}
                if(dateObj){ 
                    return dateObj.getUTCDate() + '/' + dateObj.getUTCMonth() + '/' + dateObj.getUTCFullYear();
                }else{
                    return null;
                }
            }
        },
        {
            dataField: 'action',
            isDummyField: true,
            text: '',
            headerStyle: (colum, colIndex) => {return { width: '60px', textAlign: 'center' };},
            formatter: (cell, row) => (<Button size='sm' variant="outline-danger" onClick={() => handleDeleteRow(row.id)}><BsTrash/></Button>),
            headerFormatter: () => (<Button size='sm' variant="outline-success" onClick={props.onShowModle}><BsFileEarmarkPlus/></Button>)
        }
    ];

    const handleDeleteRow = (id) =>{
        const abortConstant = new AbortController();
        deleteProject(id, abortConstant);
        setProjects(projects => projects.filter((project) => project.id !== id));
        return () => abortConstant.abort();
    };

    const deleteProject = (id, abortConstant) =>{
        fetch(props.endpoint + id,{signal: abortConstant.signal, method: 'DELETE'})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                setIsPending(false);
            })
            .catch((err) =>{
                if(err.name !== 'AbortError'){
                    setIsPending(false);
                }
            });
    };

    const rowEvents = {
        onClick: (e, row) => {
            if(e.target.cellIndex !== undefined && e.target.cellIndex !== 2){
                history.push('/project/'+row.id);
            }
        }
    };

    return (
        <div className="projectTable" style={{boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'}}>
            <BootstrapTable
                keyField='id' 
                data={projects}
                columns={columns}
                rowEvents={rowEvents}
                noDataIndication={<Spinner show={isPending}/>}
                remote={isPending}
                tabIndexCell
                striped
                hover
                condensed/>
        </div>
    );
  }
  
  export default ProjectTable;