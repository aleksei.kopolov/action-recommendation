import {Navbar} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function AppNavBar() {
    return (
        <Navbar bg="primary" expand="lg">
            <Navbar.Brand>
                <Link to='/' style={{color: 'white'}}>ProLift</Link>
            </Navbar.Brand>
        </Navbar>
    );
  }
  
  export default AppNavBar;