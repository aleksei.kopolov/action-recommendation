import {Card, Accordion, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next';
import Spinner from '../../../common/spinner/Spinner';
import Select from 'react-select'
import "./columnconfiguration.css";

function ColumnConfiguration(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const COLUMN_TYPE_INDEX = 'COLUMN_TYPE';
    const COLUMN_DATA_TYPE_INDEX = 'COLUMN_DATA_TYPE';

    const {id} = useParams();
    const [isPending, setIsPending] = useState(true);
    const [columns, setColumns] = useState([]);
    const [data, setData] = useState([]);

    useEffect(() => {
        const abortConstant = new AbortController();
        setPojectColumns(abortConstant);
        return () => abortConstant.abort();
    }, []);

    const handleSelectChange = (value, picklist, id, columns) => {
        var column = null;

        for(var i = 0; i < columns.length; i++){
            if(columns[i].id === id){
                columns[i][picklist.toLowerCase()] = value;
                column = columns[i];
                break;
            }
        }

        if(!column){return;}
        setData(getData(columns));
        props.onColumnChange(columns);
        const abortConstant = new AbortController();
        updateColumnConfig(abortConstant, column);
        return () => abortConstant.abort();
    };

    const updateColumnConfig = (abortConstant, column) =>{
        fetch(props.endpoint + 'columnmetadata/', {
            signal: abortConstant.signal,
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(column)
        });
    };

    const getData = (columns) => {
        var data = [
            {id: COLUMN_TYPE_INDEX},
            {id: COLUMN_DATA_TYPE_INDEX}
        ];

        if(!columns){return data;}

        columns.forEach(function(column){
            data[0][column.column_name] = column.column_type;
            data[1][column.column_name] = column.column_data_type;
        })

        return data;
    }

    const getTableColumns = (data) =>{
        var tableColumns = [];

        if(!data){return tableColumns;}

        tableColumns.push(
            {
                dataField: 'id',
                text: 'Id',
                hidden: true,
                headerClasses: 'align-middle',
                classes: 'align-middle'
            }
        );
        
        data.columns.forEach(function(column){
            tableColumns.push(
                {
                    dataField: column.column_name,
                    text: column.column_name,
                    hidden: false,
                    headerClasses: 'align-middle',
                    classes: 'align-middle',
                    style: () => {return{'minWidth': '250px'}},
                    formatter: (cell, row) => (
                        <div>
                            {row.id === COLUMN_TYPE_INDEX && <Select 
                                menuPosition="fixed"
                                placeholder="Column Type"
                                onChange={event => handleSelectChange(event.value, row.id, column.id, data.columns)}
                                value={cell && {'label' : cell, 'value' : cell}}
                                options={data.column_types}/>}
                            {row.id === COLUMN_DATA_TYPE_INDEX && <Select 
                                menuPosition="fixed" 
                                placeholder="Data Type"
                                onChange={event => handleSelectChange(event.value, row.id, column.id, data.columns)}
                                value={cell && {'label' : cell, 'value' : cell}}
                                options={data.column_data_types}/>}
                        </div>
                    )
                }
            );
        });

        return tableColumns;
    }

    const setPojectColumns = (abortConstant) =>{
        fetch(props.endpoint + 'projectinfo/' + id + '/columns', {signal: abortConstant.signal})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                props.onColumnChange(data.columns);
                setColumns(getTableColumns(data));
                setData(getData(data.columns));
                setIsPending(false);
            })
            .catch((err) =>{
                setIsPending(false);
                if(err.name !== 'AbortError'){}
            });
    };

    return (
        <div className="columnconfigurationmodule">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Column Configuration
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'0px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            {(!isPending && columns && columns.length > 0 && 
                            <Container fluid={true}>
                                <Row className='mt-4 mb-4'>
                                    <Col>
                                        <Accordion defaultActiveKey="1">
                                            <Card bg='white'>
                                                <Card.Header style={cardHeaderCSS}>
                                                    <Accordion.Toggle as={Card.Header} eventKey="1">
                                                        Description
                                                    </Accordion.Toggle>
                                                </Card.Header>
                                                <Accordion.Collapse eventKey="1">
                                                    <Card.Body>
                                                        <div>
                                                            Specify column type and data type of columns that you are planning to use in uplift model.
                                                        </div>
                                                        <div className='ml-2'>
                                                            <b>Column Types:</b>
                                                            <ul>
                                                                <li>Uncontrollable - Fields that can not be changed to influence the outcome</li>
                                                                <li>Controllable - Fields that can be changed to influence the outcome</li>
                                                                <li>Outcome - Fields that idicate the outcome</li>
                                                                <li>Group Identifier - Fields that are used to group records</li>
                                                                <li>Ignore - Fields that will be ignored by the model</li>
                                                                <li>Order By - Fields that are used to order records</li>
                                                            </ul>
                                                        </div>
                                                        <div className='ml-2'>
                                                            <b>Data Types</b> - data type of a given column
                                                        </div>
                                                    </Card.Body>
                                                </Accordion.Collapse>
                                            </Card>
                                        </Accordion>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{padding:'0px'}}>
                                        <BootstrapTable
                                            wrapperClasses="table-responsive"
                                            rowClasses="text-nowrap"
                                            keyField="id"
                                            data={data}
                                            columns={columns}
                                            tabIndexCell                        
                                            striped
                                            hover
                                            condensed/>
                                    </Col>
                                </Row>
                            </Container>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
  }
  
  export default ColumnConfiguration;