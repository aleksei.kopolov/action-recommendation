import {useState, useEffect} from 'react';
import {Card, Accordion} from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import Spinner from '../../../common/spinner/Spinner';
import {BsTrash, BsFileEarmarkPlus} from 'react-icons/bs';
import {Button} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';
import {useParams} from 'react-router-dom';
import CreateModelConfiguration from './CreateModelConfiguration';

function ModelTable(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [models, setModels] = useState([]);
    const [showCreateModel, setShowCreateModel] = useState(false);
    const [isPending, setIsPending] = useState(true);
    const {id} = useParams();

    const history = useHistory();

    const columns = [
        {
            dataField: 'id',
            text: 'Id',
            hidden: true,
            headerClasses: 'align-middle',
            classes: 'align-middle',
            style: () => {return{cursor: 'pointer'};}
        },
        {
            dataField: 'name',
            text: 'Model Name',
            headerTitle: true,
            headerClasses: 'align-middle',
            classes: 'align-middle',
            style: () => {return{cursor: 'pointer'};}
        },
        {
            dataField: 'status',
            text: 'Status',
            headerTitle: true,
            headerClasses: 'align-middle',
            classes: 'align-middle',
            style: () => {return{cursor: 'pointer'};}
        },
        {
            dataField: 'create_date',
            text: 'Create Date',
            headerTitle: true,
            classes: 'align-middle',
            headerClasses: 'align-middle',
            style: () => {return{cursor: 'pointer'};},
            formatter: (cell) => {
                let dateObj = cell;
                if (typeof cell !== 'object') {dateObj = new Date(cell);}
                if(dateObj){ 
                    return dateObj.getUTCDate() + '/' + dateObj.getUTCMonth() + '/' + dateObj.getUTCFullYear();
                }else{
                    return null;
                }
            }
        },
        {
            dataField: 'action',
            isDummyField: true,
            text: '',
            headerStyle: (colum, colIndex) => {return { width: '60px', textAlign: 'center' };},
            formatter: (cell, row) => (<Button size='sm' variant="outline-danger" onClick={() => handleDeleteRow(row.id)}><BsTrash/></Button>),
            headerFormatter: () => (<Button size='sm' variant="outline-success" onClick={() => setShowCreateModel(true)}><BsFileEarmarkPlus/></Button>)
        }
    ];

    const handleDeleteRow = (id) =>{
        const abortConstant = new AbortController();
        deleteProject(id, abortConstant);
        setModels(models => models.filter((model) => model.id !== id));
        return () => abortConstant.abort();
    };

    const deleteProject = (id, abortConstant) =>{
        fetch(props.endpoint + 'modelconfiguration/' + id ,{signal: abortConstant.signal, method: 'DELETE'})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                setIsPending(false);
            })
            .catch((err) =>{
                if(err.name !== 'AbortError'){
                    setIsPending(false);
                }
            });
    };

    const rowEvents = {
        onClick: (e, row) => {
            if(e.target.cellIndex !== undefined && e.target.cellIndex !== 2){
                history.push('/modelconfiguration/'+row.id);
            }
        }
    };

    useEffect(() => {
        const abortConstant = new AbortController();
        getProjectModels(abortConstant);
        return () => abortConstant.abort();
    }, [])

    const getProjectModels = (abortConstant) =>{
        fetch(props.endpoint + 'projectinfo/' + id + '/models', {signal: abortConstant.signal})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                setModels(data);
                setIsPending(false);
            })
            .catch((err) =>{
                setIsPending(false);
                if(err.name !== 'AbortError'){}
            });
    }

    return (
        <div className="modeltable">
            <CreateModelConfiguration 
                endpoint={props.endpoint}
                show={showCreateModel}
                onHide={() => setShowCreateModel(false)}/>
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Models
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'0px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            {(!isPending && 
                                <BootstrapTable
                                    keyField='id' 
                                    data={models}
                                    columns={columns}
                                    rowEvents={rowEvents}
                                    remote={isPending}
                                    tabIndexCell
                                    striped
                                    hover
                                    condensed/>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
}

export default ModelTable;