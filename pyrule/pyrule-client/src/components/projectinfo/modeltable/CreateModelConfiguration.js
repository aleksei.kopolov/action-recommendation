import {useState} from 'react';
import { useHistory } from 'react-router-dom';
import {Modal, Button, Form} from 'react-bootstrap';
import Spinner from '../../../common/spinner/Spinner';
import {useParams} from 'react-router-dom';

function CreateModelConfiguration(props){
    const [validated, setValidated] = useState(false);
    const [isPending, setIsPending] = useState(false);
    const [modelName, setModelName] = useState('');

    const history = useHistory();
    const {id} = useParams();

    const handleSubmit = (event) => {
        setValidated(true);
        event.preventDefault();
        event.stopPropagation();

        if (event.currentTarget.checkValidity() === true) {
            setIsPending(true);
            creatModelConfiguration();
            setValidated(false);
        }
    };

    const creatModelConfiguration = () =>{
        fetch(props.endpoint + 'projectinfo/' + id + '/model', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'accept': 'application/json'
                },
                body: JSON.stringify({
                    name : modelName
                })
            })
            .then(res => {
                if(!res.ok){throw Error('Creation faild');}
                return res.json();
            })
            .then(data =>{
                history.push('/modelconfiguration/' + data.id);
            })
            .catch((err) =>{
                console.log(err);
            });
    }

    return (
        <div className="createmodelconfiguration">
            <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered backdrop="static">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Create New Model Configuration
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {(isPending && <Spinner show={isPending}/>)}
                    {(!isPending && 
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group controlId="modelConfigurationName">
                                <Form.Label>Model Configuration Name</Form.Label>
                                <Form.Control
                                    required
                                    autoComplete = 'off'
                                    value={modelName}
                                    onChange={e => setModelName(newProjectName => e.target.value)}
                                    type="text"
                                    placeholder="Model Configuration Name"/>
                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">Please choose a module configuration name</Form.Control.Feedback>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                        </Form>
                        )}
                </Modal.Body>
            </Modal>
        </div>
    )
}

export default CreateModelConfiguration;