import {Card, Accordion, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import Spinner from '../../../common/spinner/Spinner';
import QueryBuilder from '../../../common/querybuilder/QueryBuilder';

function SuccessRuleConfiguration(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(true);
    const [ruleConfiguration, setRuleConfiguration] = useState({})
    const [columns, setColumns] = useState([]);
    const [query, setQuery] = useState([]);
    const {id} = useParams();

    useEffect(() => {
        setColumns(getQueryColumns(props.columns));
    }, [props.columns]);

    useEffect(() => {
        const abortConstant = new AbortController();
        getConfigurationRules(abortConstant);
        return () => abortConstant.abort();
    }, []);

    const getConfigurationRules = (abortConstant) =>{
        fetch(props.endpoint + 'projectinfo/' + id + '/resultrules', {signal: abortConstant.signal})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                setRuleConfiguration(data);
                setQuery(buildQueryTree(data.rules));
                setIsPending(false);
            })
            .catch((err) =>{
                if(err.name !== 'AbortError'){}
            });
    }

    const processRule = (rule) =>{
        return rule.filter(r => {
            if(r.value === 'True' || r.value === 'False'){
                r.value = (r.value === 'True')
            }
            return r;
        })
    }

    const buildTreeChildren = (rules) =>{
        let result = [];
        rules.forEach(rule => {
            result.push({
                    'type' : 'GROUP',
                    'value' : 'AND',
                    'isNegated': false,
                    'children' : processRule(rule)
                }
            )
        })
        return result;
    };

    const buildQueryTree = (rules) =>{
        return [{
            'type' : 'GROUP',
            'value' : 'AND',
            'isNegated': false,
            'children' : buildTreeChildren(rules)
        }];
    };

    const processRules = (rules) =>{
        return rules.filter(r => {
            if(r.value === true){
                r.value = 'True';
                r.operator = "=";
            }else if (r.value === false){
                r.value = 'False';
                r.operator = "=";
            }
            return r;
        })
    }

    const getRules = (tree) =>{
        let rules = [];
        tree[0].children.forEach(chield => { 
            rules.push(processRules(chield.children));
        });
        return rules;
    };

    const objectsAreSame = (x, y) => {
        for(var element in x) {
            if(x[element] && y[element]){
                for(var propertyName in x[element]) {
                    if(x[element][propertyName] !== y[element][propertyName]){
                        return false;
                    }
                }
            }else{
                return false;
            }
        }
        return true;
    };


    const canUpdateProcess = (oldRules, newRules) => {
        return (oldRules.length !== newRules.length || !objectsAreSame(oldRules, newRules))
    };

    const handleQueryUpdate = (data) =>{
        if(canUpdateProcess(data[0].children, query[0].children)){
            setQuery(data);
            let ruleConfigurationDto = {
                'id' : ruleConfiguration.id,
                'rules' : getRules(data)
            }
            updateRuleConfiguration(ruleConfigurationDto);
        }
    };

    const updateRuleConfiguration = (ruleConfigurationDto) =>{
        fetch(props.endpoint + 'ruleconfiguration/', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(ruleConfigurationDto)
        })
        .then(res => { if(!res.ok){throw Error('Creation faild');} })
        .catch((err) =>{ console.log(err); });
    };

    const getQueryColumns = (columns) =>{
        let fields = []

        columns.forEach(column => {
            let filed = {
                'field' : column.column_name,
                'label' : column.column_name,
                'type' : column.column_data_type,
                'operators' : props.operators[column.column_data_type]
            };

            if(column.column_type && column.column_type !== 'IGNORE' && column.column_type === 'OUTCOME'){
                if(filed.type === 'CATEGORICAL'){filed.type = 'TEXT';}
                fields.push(filed)
            }
        })
        return fields;
    };

    return (
        <div className="successruleconfigurationmodule">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Outcome Configuration
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'0px'}}>
                            {(isPending && <Spinner show={true}/>)}
                            {(!isPending && 
                            <Container fluid={true}>
                                <Row className='mt-4 mb-4'>
                                    <Col className='ml-1'>
                                        <Card>
                                            <Card.Body>
                                                Specifay conditions that indicate a positive outcome.
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col style={{padding:'0px'}}>
                                        <QueryBuilder
                                            data={query}
                                            onQueryUpdate={handleQueryUpdate}
                                            columns={columns}/>
                                    </Col>
                                </Row>
                            </Container>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    )
}

export default SuccessRuleConfiguration;