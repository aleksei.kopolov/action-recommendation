import {useState} from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import FileModule from './filemodule/FileModule';
import ColumnConfiguration from './columnconfiguration/ColumnConfiguration';
import SuccessRuleConfiguration from './successruleconfiguration/SuccessRuleConfiguration';
import TreatmentRuleConfiguration from './treatmentruleconfiguration/TreatmentRuleConfiguration';
import ModelTable from './modeltable/ModelTable';

function ProjectInfo(props) {
    const endpoint = props.endpoint;
    const [isFilePresent, setIsFilepresent] = useState(false);
    const [columns, setColumns] = useState([]);

    const handleChangeColumns = (newColumns) =>{
        setColumns([...newColumns]);
    }

    const operators = {
        "CATEGORICAL": ["EQUAL", "NOT_EQUAL"],
        "TEXT": ["EQUAL", "NOT_EQUAL"],
        "NUMBER": ["EQUAL", "NOT_EQUAL", "LARGER", "SMALLER", "LARGER_EQUAL", "SMALLER_EQUAL"],
        "BOOLEAN": ["EQUAL", "NOT_EQUAL"],
        "DATE": ["EQUAL", "NOT_EQUAL", "LARGER", "SMALLER", "LARGER_EQUAL", "SMALLER_EQUAL"]
    }

    return (
        <div className="project">
            <Container fluid={true}>
                <Row className='mt-4'>
                    <Col>
                        <FileModule endpoint={endpoint} onFileUploaded={() => setIsFilepresent(true)}/>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(isFilePresent && <ColumnConfiguration endpoint={endpoint} onColumnChange={handleChangeColumns}/>)}
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(isFilePresent && columns.length > 0 && <SuccessRuleConfiguration endpoint={endpoint} operators={operators} columns={columns}/>)}
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(isFilePresent && columns.length > 0 && <TreatmentRuleConfiguration endpoint={endpoint} operators={operators} columns={columns}/>)}
                    </Col>
                </Row>
                <Row className='mt-4 mb-5'>
                    <Col>
                        {(isFilePresent && columns.length > 0 && <ModelTable endpoint={endpoint}/>)}
                    </Col>
                </Row>
            </Container>
        </div>
    );
  }
  
  export default ProjectInfo;