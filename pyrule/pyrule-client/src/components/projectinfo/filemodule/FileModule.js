import {useState, useEffect} from 'react';
import {Card, Accordion} from 'react-bootstrap';
import {useParams, useHistory} from 'react-router-dom';
import FileInfo from './FileInfo';
import Spinner from '../../../common/spinner/Spinner';
import FileUpload from './FileUpload';


function FileModule(props){
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};

    const cardHeaderCSS = {
        padding:'0px', 
        cursor: 'pointer',
        "&:hover": { background: 'rgba(0, 0, 0, 0.06)' }
    };

    const [projectInfo, setProjectInfo] = useState({
        project_name : ' ',
        has_file : true,
        file_status : ' ',
        file_name : ' ',
        file_size : 0,
        file_upload_date : ' '
    });

    const [isPending, setIsPending] = useState(true);
    const {id} = useParams()

    const endpoint = props.endpoint + 'projectinfo/' + id;
    const fileUploadEndpoint = props.endpoint + 'uploadprojectfile/' + id;

    const history = useHistory();

    const abortConstant = new AbortController();
    let info_timer = null;

    useEffect(() => {
        getProjectInfo(abortConstant);
        return () => {
            abortConstant.abort();
            clearTimeout(info_timer);
        }
    }, []);

    const getProjectInfo = (abort) => {
        fetch(endpoint, {signal: abort.signal})
            .then(res => {
                if(!res.ok){throw Error('could not fetch the data');}
                return res.json();
            })
            .then(data =>{
                validateProjectInfoData(data);
                setIsPending(false);
            })
            .catch((err) =>{
                setProjectInfo({});
                setIsPending(false);
                history.push('/');
            });
    };

    const validateProjectInfoData = (data) =>{
        setProjectInfo(data);
        if(data){
            if(data.is_uploading){
                info_timer = setTimeout(() => {
                    getProjectInfo(abortConstant)
                }, 5000)
            }

            if(data.has_columns){
                props.onFileUploaded();
            }
        }
    };

    return (
        <div className='filecard'>
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Name: {projectInfo.project_name}
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            <Spinner show={isPending}/>
                            {(!isPending && !projectInfo.has_file && <FileUpload endpoint={fileUploadEndpoint} onFileUploaded={validateProjectInfoData}/>)}
                            {(!isPending && projectInfo.has_file && <FileInfo projectInfo={projectInfo}/>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    )
}

export default FileModule;