import {Row, Col, Container} from 'react-bootstrap';
import {TextField} from '@material-ui/core';
import './fileinfo.css'

function FileInfo(props) {
    const project_data = props.projectInfo;

    return (
        <div className="projectinfoview">
            <Container fluid={true}>
                <Row>
                    <Col>
                        <TextField label='File name' fullWidth disabled value={project_data.file_name}/>
                    </Col>
                    <Col>
                        <TextField label='File Size(MB)' fullWidth disabled value={project_data.file_size}/>
                    </Col>

                    {(project_data.is_uploading && 
                        <Col>
                            <TextField label='File Status' fullWidth disabled value={project_data.file_status + ' : ' + project_data.uploading_percentage + '%'}/>
                        </Col>
                    )}

                    {(!project_data.is_uploading && 
                        <Col>
                            <TextField label='File Status' fullWidth disabled value={project_data.file_status}/>
                        </Col>
                    )}

                    {(project_data.is_uploading && 
                    <Col>
                        <TextField label='Upload Date' fullWidth disabled value={' '}/>
                    </Col>
                    )}

                    {(!project_data.is_uploading && 
                        <Col>
                            <TextField label='Upload Date' fullWidth disabled value={project_data.file_upload_date}/>
                        </Col>
                    )}
                </Row>
            </Container>
        </div>
    );
  }
  
  export default FileInfo;