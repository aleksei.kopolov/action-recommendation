import {useState, useMemo} from 'react'
import {Row, Container, Button, ProgressBar} from 'react-bootstrap';
import {useDropzone} from 'react-dropzone'
import axios from 'axios';

function FileUpload(props) {
    const endpoint = props.endpoint;

    const [selectedFile, setSelectedFile] = useState({});
    const [isFileSelected, setIsFileSelected] = useState(false);
    const [isUploading, setIsUploading] = useState(false);
    const [uploadProgress, setUploadProgress] = useState(0);

    const baseStyle = {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out'
    };
      
    const activeStyle = {
        borderColor: '#2196f3'
    };
      
    const acceptStyle = {
        borderColor: '#00e676'
    };
      
    const rejectStyle = {
        borderColor: '#ff1744'
    };

    const {getRootProps, getInputProps, acceptedFiles, isDragActive, isDragAccept, isDragReject} = useDropzone({
        accept: 'text/csv',
        multiple: false,
        maxSize: 524288001,
        onDrop: (acceptedFiles) =>{
            if(acceptedFiles && acceptedFiles.length > 0){
                setSelectedFile(acceptedFiles[0]);
                setIsFileSelected(true);
            }
        }
    });

    const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [isDragActive, isDragReject, isDragAccept]);

    const cancelUpload = () =>{
        setSelectedFile({});
        setIsFileSelected(false);
    };

    const startUpload = () => {
        const abortConstant = new AbortController();
        setIsUploading(true);
        uploadFile(abortConstant);
        return () => abortConstant.abort();
    };

    const uploadFile = (abortConstant) =>{
        const formData = new FormData()
        formData.append('upload_file', selectedFile);

        axios.post(endpoint, formData, {
            onUploadProgress: function(progressEvent){
                setUploadProgress(Math.round( (progressEvent.loaded * 100) / progressEvent.total ));
            }
        })
        .then(res => {
            if(res.status === 200){
                props.onFileUploaded(res.data);
                setIsUploading(false);
            }else{
                throw Error('Upload faild');
            }
        })
        .catch((err) =>{
            setIsUploading(false);
            console.log(err);
        });
    };

    return (
        <div className="fileUpload">
            <Container fluid={true}>
                {(!isFileSelected && !isUploading &&
                    <Row>
                        <div {...getRootProps({style})}>
                            <input {...getInputProps()} />
                            <p>Drag 'n' drop some files here, or click to select files</p>
                            <em>(*.csv files max 500 MB)</em>
                        </div>
                    </Row>
                )}
                {(isFileSelected && !isUploading &&
                    <Row>
                        {selectedFile.name} - {(selectedFile.size / (1024*1024)).toFixed(2)} Mb
                    </Row>
                )}
                {(isFileSelected && !isUploading &&
                    <Row className="mt-3">
                        <Button variant="outline-danger" onClick={cancelUpload}>
                                Cancel
                        </Button>
                        <Button variant="outline-success" className="ml-3" onClick={startUpload}>
                                Upload
                        </Button>
                    </Row>
                )}
                {(isUploading && 
                    <Row>
                        <ProgressBar 
                        style={{width: "100%"}}
                        variant="success"
                        animated 
                        now={uploadProgress}/>
                    </Row>
                )}
            </Container>
        </div>
    );
  }
  
  export default FileUpload;