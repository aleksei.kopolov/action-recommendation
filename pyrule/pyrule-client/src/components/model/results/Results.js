import {useState, useEffect, forwardRef, useImperativeHandle} from 'react';
import {Card, Accordion, ListGroup} from 'react-bootstrap';
import Spinner from '../../../common/spinner/Spinner';
import {useParams} from 'react-router-dom';
import parse from 'html-react-parser';


const Results = forwardRef((props, ref) => {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(true);
    const [results, setResults] = useState([]);
    const [modelConfiguration] = useState({treatment_rules:[], result_rules:[]});
    const {id} = useParams();
    let info_timer = null;

    useEffect(() => {
        setIsPending(true);
        const abortConstant = new AbortController();
        getResults(abortConstant);
        return () => {
            abortConstant.abort();
            clearTimeout(info_timer);
        }
    },[]);

    useImperativeHandle(ref, () => ({
        reload() { 
            setIsPending(true);
            const abortConstant = new AbortController();
            getResults(abortConstant);
            return () => {
                abortConstant.abort();
                clearTimeout(info_timer);
            }   
        }
    }))

    useEffect(() => {
        modelConfiguration.treatment_rules = props.modelData.treatment_rules;
        modelConfiguration.result_rules = props.modelData.result_rules;
    },[props.modelData]);

    const getResults = (abortConstant) =>{
        fetch(props.endpoint + 'resultrules/' + id, {
            signal: abortConstant.signal,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{
            if(data.is_loading){
                info_timer = setTimeout(() => {getResults(abortConstant)}, 5000)
            }else{
                setIsPending(false);
                var resultRules = data.result_rules;
                resultRules.sort((a, b) => b.uplift_score - a.uplift_score);
                setResults(resultRules);
            }
        })
        .catch((err) =>{})
    };

    const getOperator = (operator) => {
        if(operator === 'LARGER'){
            return 'larger than';
        }else if(operator === 'SMALLER'){
            return 'smaller than';
        }else if(operator === 'EQUAL'){
            return 'equal to';
        }else if(operator === 'NOT_EQUAL'){
            return 'not equal to';
        }else if(operator === 'LARGER_EQUAL'){
            return 'larger or equal to';
        }else if(operator === 'SMALLER_EQUAL'){
            return 'smaller or equal to';
        }else{
            return operator;
        }
    }

    const getRuleDescription = (rules) =>{
        var condition = '';
        rules.forEach(rule => {
            if(condition === ''){
                condition += '<b>' + rule.field + '</b> field is ' + getOperator(rule.operator) + ' <u>' + rule.value + '</u>'
            }else{
                condition += ' and <b>' + rule.field + '</b> is ' + getOperator(rule.operator) + ' <u>' + rule.value + '</u>'
            }
        });
        return condition;
    }

    const getRuleScore = (uplift_score) =>{
        var header = uplift_score > 0 ? 'The chance of a positive outcome increases by ' + getScorePrecentage(uplift_score) + '%' : 'The chance of a positive outcome decreases by ' + getScorePrecentage(uplift_score) + '%';
        return header;
    }

    const getScorePrecentage = (score) =>{
        return Math.round((score * 100.0 + Number.EPSILON) * 100) / 100
    }

    const getRuleOrder = (value) =>{
        var orderName = '';
        if(value === 1){
            orderName = '1st ';
        }else if(value === 2){
            orderName = '2nd ';
        }else {
            orderName += value + 'th ';
        }
        orderName += 'action - ';
        return orderName;
    }

    return (
        <div className="results">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Results
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            {(!isPending && <div>
                                <div>
                                    <p style={{'marginBottom': 0, 'marginTop':'10px'}}>Based on the provided positive outcome rules:</p>
                                    <ListGroup variant="flush">
                                        {modelConfiguration.result_rules.map((rule_set, index) => 
                                            <ListGroup.Item key={'outcome-' + index}>
                                                {parse('<p style="margin: 0">' + getRuleDescription(rule_set) + '</p>')}
                                            </ListGroup.Item>
                                        )}
                                    </ListGroup>
                                </div>
                                <div>
                                    <p style={{'marginBottom': 0, 'marginTop':'10px'}}>and on the provided treatment rules where the:</p>
                                    <ListGroup variant="flush">
                                        {modelConfiguration.treatment_rules.map((rule_set, index) => 
                                            <ListGroup.Item key={'treatment-' + index}>
                                                {parse('<p style="margin: 0">' + getRuleOrder(index+1) + getRuleDescription(rule_set) + '</p>')}
                                            </ListGroup.Item>
                                        )}
                                    </ListGroup>
                                </div>
                                <div>
                                    <p style={{'marginBottom': 0, 'marginTop':'10px'}}>The application of the treatment rules has the following affect:</p>
                                </div>
                                <ListGroup variant="flush">
                                    {results.map((result, index) => 
                                        <ListGroup.Item variant={result.uplift_score > 0 ? 'success' : 'danger'} key={'rule-'+index}>
                                            <div className="ms-2 me-auto">
                                                {parse('<p style="margin: 0"> For cases where: ' + getRuleDescription(result.rules) + '</p>')}
                                                {getRuleScore(result.uplift_score)}
                                            </div>
                                        </ListGroup.Item>
                                    )}
                                </ListGroup>
                            </div> )}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
})

export default Results;