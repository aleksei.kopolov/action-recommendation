import {useState, useEffect} from 'react';
import {Card, Accordion} from 'react-bootstrap';
import Spinner from '../../../common/spinner/Spinner';
import {useHistory} from 'react-router-dom';
import {useParams} from 'react-router-dom';

function DataConfigurationInfo(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(false);
    const {id} = useParams();

    const history = useHistory();


    return (
        <div className="dataconfigurationinfo">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Data Configuration
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            Add data configuration here
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
}

export default DataConfigurationInfo;