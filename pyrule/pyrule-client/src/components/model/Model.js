import {useState, useEffect, useRef} from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import {useParams} from 'react-router-dom';
import DataAnalytics from './dataanalytics/DataAnalytics';
import DataBalancing from './databalancing/DataBalancing';
import ModelSettings from './modelsettings/ModelSettings';
import ConfusionMatrix from './confusionmatrix/ConfusionMatrix';
import Results from './results/Results';

function Model(props) {
    const endpoint = props.endpoint;
    const resultsRef = useRef(null);
    const dataAnalyticsRef = useRef(null);
    const confusionMatrixRef = useRef(null);

    const [isPending, setIsPending] = useState(true);
    const [data, setData] = useState({
        data_balancing:{},
        model_settings:{},
        treatment_rules:[],
        result_rules:[]
    });

    const [process, setProcess] = useState({
        is_processing: false,
        process_percentage: 0.0,
        status: ''
    })

    const {id} = useParams();
    let info_timer = null;

    useEffect(() => {
        setIsPending(true);
        const abortConstant = new AbortController();
        getModelConfigurationData(abortConstant);
        return () => {
            abortConstant.abort();
            clearTimeout(info_timer);
        }
    }, []);

    const getModelConfigurationData = (abortConstant) =>{
        fetch(props.endpoint + 'modelconfiguration/' + id, {
            signal: abortConstant.signal,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{
            console.log(data);
            if(data.process.is_processing){
                setProcess(data.process);
                info_timer = setTimeout(() => {getModelConfigurationData(abortConstant)}, 5000);
            }else{
                setData(data);
                setIsPending(false);
                dataAnalyticsRef.current.setAnalyticsData()
            }
        })
        .catch((err) =>{
            setIsPending(false);
            if(err.name !== 'AbortError'){}
        })        
    };

    const setModelSettings = (modelSettings) =>{
        const abortConstant = new AbortController();
        data.model_settings = modelSettings;
        buildModel(abortConstant);
        return () => abortConstant.abort();
    };

    const buildModel = (abortConstant) =>{
        fetch(props.endpoint + 'modelbuilding/' + id + '/buildmodel', {
            signal: abortConstant.signal,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(data.model_settings)
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{resultsRef.current.reload()})
        .catch((err) =>{})
    }

    const setDataBalancing = (data_balancing) =>{
        const abortConstant = new AbortController();
        data.data_balancing = data_balancing;
        balanceData(abortConstant);
        return () => abortConstant.abort();
    };

    const balanceData = (abortConstant) =>{
        fetch(props.endpoint + 'modelbuilding/' + id + '/balancedata', {
            signal: abortConstant.signal,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
            body: JSON.stringify(data.data_balancing)
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{confusionMatrixRef.current.reload()})
        .catch((err) =>{})
    }

    return (
        <div className="modelconfiguration">
            <Container fluid={true}>
                <Row className='mt-4'>
                    <Col>
                        <DataAnalytics ref={dataAnalyticsRef} endpoint={endpoint} process={process}/>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(!isPending && <DataBalancing endpoint={endpoint} configuration={data.data_balancing || ''} onBalanceData={setDataBalancing}/>)}
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(!isPending && <ConfusionMatrix ref={confusionMatrixRef} endpoint={endpoint} configuration={data.data_balancing || ''}/>)}
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {(!isPending && <ModelSettings endpoint={endpoint} configuration={data.model_settings || ''} onSetModelSettings={setModelSettings}/>)}
                    </Col>
                </Row>
                <Row className='mt-4 mb-5'>
                    <Col>
                        {(!isPending && <Results endpoint={endpoint} modelData={data} ref={resultsRef}/>)}
                    </Col>
                </Row>
            </Container>
        </div>
    );
  }
  
  export default Model;