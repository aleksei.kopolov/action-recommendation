import {useState, useEffect} from 'react';
import {Card, Accordion, Row, Col, Container} from 'react-bootstrap';
import Spinner from '../../../common/spinner/Spinner';
import {TextField} from '@material-ui/core';
import {Button} from 'react-bootstrap';

function DataBalancing(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(false);
    const [cutOffPercentile, setCutOffPercentile] = useState(0);
    const [balanceMaxDelta, setBalanceMaxDelta] = useState(0);

    useEffect(() => {
        setCutOffPercentile(props.configuration.cut_off_percentile);
        setBalanceMaxDelta(props.configuration.balance_max_delta);
        setIsPending(false);
    }, [props.configuration]);

    const balanceData = () => {
        props.onBalanceData({
            cut_off_percentile : cutOffPercentile,
            balance_max_delta : balanceMaxDelta,
            confusion_matrix: props.configuration.confusion_matrix
        });
    };

    return (
        <div className="databalancing">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Data Balancing
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            {(!isPending && <Container fluid={true}>
                                <Row>
                                    <Col>
                                        <TextField label='Cut-off Percentile' variant="outlined" fullWidth value={cutOffPercentile || ''} onChange={(e) => {setCutOffPercentile(e.target.value)}}/>
                                    </Col>
                                    <Col>
                                        <TextField label='Max Delta' variant="outlined" fullWidth value={balanceMaxDelta || ''} onChange={(e) => {setBalanceMaxDelta(e.target.value)}}/>
                                    </Col>
                                </Row>
                            </Container>)}
                            {(!isPending && <div style={{display:'flex', justifyContent:'center', alignItem:'center', marginTop:'30px'}}>
                                <Button variant="outline-primary" size="lg" onClick={balanceData}>Balance Data</Button>
                            </div>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
}

export default DataBalancing;