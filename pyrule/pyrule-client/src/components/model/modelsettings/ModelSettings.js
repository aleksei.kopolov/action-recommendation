import {useState, useEffect} from 'react';
import {Card, Accordion, Row, Col, Container} from 'react-bootstrap';
import Spinner from '../../../common/spinner/Spinner';
import {TextField} from '@material-ui/core';
import {Button} from 'react-bootstrap';

function ModelSettings(props) {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(true);
    const [evaluationFunction, setEvaluationFunction] = useState('');
    const [maxDepth, setMaxDepth] = useState(0);
    const [minSamplesLeaf, setMinSamplesLeaf] = useState(0);
    const [minSamplesTreatment, setMinSamplesTreatment] = useState(0);
    const [nReg, setNReg] = useState(0);

    useEffect(() => {
        setEvaluationFunction(props.configuration.evaluationFunction);
        setMaxDepth(props.configuration.max_depth);
        setMinSamplesLeaf(props.configuration.min_samples_leaf);
        setMinSamplesTreatment(props.configuration.min_samples_treatment);
        setNReg(props.configuration.n_reg);
        setIsPending(false);
    }, [props.configuration]);

    const buildModel = () => {
        props.onSetModelSettings({
            evaluationFunction : evaluationFunction,
            max_depth : maxDepth,
            min_samples_leaf : minSamplesLeaf,
            min_samples_treatment : minSamplesTreatment,
            n_reg : nReg
        });
    };

    return (
        <div className="modelsettings">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Model Settings
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            {(isPending && <Spinner show={isPending}/>)}
                            {(!isPending && <Container fluid={true}>
                                <Row>
                                    <Col>
                                        <TextField label='Evaluation Function' variant="outlined" fullWidth value={evaluationFunction || ''} onChange={(e) => {setEvaluationFunction(e.target.value)}}/>
                                    </Col>
                                    <Col>
                                        <TextField label='Max Depth' variant="outlined" fullWidth value={maxDepth || ''} onChange={(e) => {setMaxDepth(e.target.value)}}/>
                                    </Col>
                                    <Col>
                                        <TextField label='Min Samples Leaf' variant="outlined" fullWidth value={minSamplesLeaf || ''} onChange={(e) => {setMinSamplesLeaf(e.target.value)}}/>
                                    </Col>
                                    <Col>
                                        <TextField label='Min Samples Treatment' variant="outlined" fullWidth value={minSamplesTreatment || ''} onChange={(e) => {setMinSamplesTreatment(e.target.value)}}/>
                                    </Col>
                                </Row>
                                <Row className='mt-4'>
                                    <Col>
                                        <TextField label='N Reg' variant="outlined" fullWidth value={nReg || ''} onChange={(e) => {setNReg(e.target.value)}}/>
                                    </Col>
                                    <Col></Col>
                                    <Col></Col>
                                    <Col></Col>
                                </Row>
                            </Container>)}
                            {(!isPending && <div style={{display:'flex', justifyContent:'center', alignItem:'center', marginTop:'30px'}}>
                                <Button variant="outline-primary" size="lg" onClick={buildModel}>Build model</Button>
                            </div>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
}

export default ModelSettings;