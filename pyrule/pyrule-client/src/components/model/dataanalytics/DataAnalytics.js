import {useState, forwardRef, useImperativeHandle} from 'react';
import {Card, Accordion, Row, Col, Container} from 'react-bootstrap';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush, Title} from 'recharts';
import Spinner from '../../../common/spinner/Spinner';
import {useParams} from 'react-router-dom';
    
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';


const DataAnalytics = forwardRef((props, ref) => {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [isPending, setIsPending] = useState(true);

    const [prefixAnaliticsData, setPrefixAnaliticsData] = useState([]);

    const {id} = useParams();

    useImperativeHandle(ref, () => ({
        setAnalyticsData(){
            setIsPending(true);
            const abortConstant = new AbortController();
            getPrefixData(abortConstant);
            return () => abortConstant.abort();
        }
    }))

    const getPrefixData = (abortConstant) =>{
        fetch(props.endpoint + 'prefixanalitics/' + id, {
            signal: abortConstant.signal,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{
            if(data){
                setPrefixAnaliticsData(data);
                setIsPending(false);
            }
        })
        .catch((err) =>{
            setIsPending(false);
            if(err.name !== 'AbortError'){}
        })        
    };

    return (
        <div className="dataanalytics">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Data Analytics
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                            {(isPending && props.process.process_percentage > 0 &&
                            <div style={{ width: '150px', height: '190px', marginLeft: 'auto', marginRight: 'auto'}}>
                                <h4 style={{textAlign: 'center'}}>Processing</h4>
                                <div style={{width: '150px', height: '100px'}}>
                                    <CircularProgressbar value={props.process.process_percentage} text={`${props.process.process_percentage}%`} />
                                </div>
                            </div>)}
                            {(isPending && props.process.process_percentage === 0 && <Spinner show={isPending}/>)}
                            {(!isPending && <div style={{margin:'15px'}}>
                                <Container fluid={true}>
                                    <Row>
                                        <Col className='mb-4'>
                                            <Card>
                                                <Card.Body>
                                                    <div style={{textAlign:'center'}}>
                                                        <br/>
                                                        Stacked chart indicating the number all types of outcomes in relation to the length of a given case
                                                    </div>
                                                    <div style={{display:'flex', justifyContent:'center', alignItem:'center'}}>
                                                        <BarChart width={1100} height={500} data={prefixAnaliticsData.case_size_analitics}>
                                                            <CartesianGrid strokeDasharray="3 3" />
                                                            <XAxis dataKey="index"></XAxis>
                                                            <YAxis/>
                                                            <Tooltip />
                                                            <Legend />
                                                            <Brush dataKey="name" height={30} stroke="#8884d8" />
                                                            <Bar dataKey="has_treatment" stackId="a" fill="#277da1" />
                                                            <Bar dataKey="missing_treatment" stackId="a" fill="#f8961e" />
                                                            <Bar dataKey="success" stackId="a" fill="#90be6d" />
                                                            <Bar dataKey="failed" stackId="a" fill="#f94144" />
                                                        </BarChart>
                                                    </div>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                        <Col className='mb-4'>  
                                            <Card>
                                                <Card.Body>
                                                <div style={{textAlign:'center'}}>
                                                    Stacked chart indicating the number of positive/negative outcomes in relation to the number of interactions that a given case 
                                                    had before all of the treatment rules have been played
                                                </div>
                                                    <div style={{display:'flex', justifyContent:'center', alignItem:'center'}}>
                                                        <BarChart width={1100} height={500} data={prefixAnaliticsData.case_treatment_analitics}>
                                                            <CartesianGrid strokeDasharray="3 3" />
                                                            <XAxis dataKey="index"></XAxis>
                                                            <YAxis />
                                                            <Tooltip />
                                                            <Legend />
                                                            <Brush dataKey="name" height={30} stroke="#8884d8" />
                                                            <Bar dataKey="success" stackId="a" fill="#90be6d" />
                                                            <Bar dataKey="failed" stackId="a" fill="#f94144" />
                                                        </BarChart>
                                                    </div>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    </Row>
                                </Container>
                            </div>)}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
})

export default DataAnalytics;