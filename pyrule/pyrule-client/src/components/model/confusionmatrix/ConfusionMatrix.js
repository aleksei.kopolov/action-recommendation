import {useState, forwardRef, useImperativeHandle, useEffect} from 'react';
import {Card, Accordion} from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Spinner from '../../../common/spinner/Spinner';

const ConfusionMatrix = forwardRef((props, ref) => {
    const cardCSS = {boxShadow: 'rgba(0,0,0,.125) 0px 10px 15px -3px, rgba(0,0,0,.125) 0px 4px 6px -2px'};
    const cardHeaderCSS = {padding:'0px', cursor: 'pointer', "&:hover": {background: 'rgba(0, 0, 0, 0.06)'}};

    const [confusionMatrix, setConfusionMatrix] = useState([]);
    const [isPending, setIsPending] = useState(true);
    const {id} = useParams();
    let info_timer = null;

    useEffect(() => {
        setConfusionMatrix(props.configuration.confusion_matrix);
        setIsPending(false);
    }, [props.configuration]);

    useImperativeHandle(ref, () => ({
        reload() { 
            setIsPending(true);
            const abortConstant = new AbortController();
            info_timer = setTimeout(() => {getConfusionMatrix(abortConstant)}, 2500);
            return () => {
                abortConstant.abort();
                clearTimeout(info_timer);
            }
        }
    }))

    const getConfusionMatrix = (abortConstant) =>{
        fetch(props.endpoint + 'modelconfiguration/' + id + '/confusionmatrix', {
            signal: abortConstant.signal,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            },
        })
        .then(res => {
            if(!res.ok){throw Error('could not fetch the data');}
            return res.json();
        })
        .then(data =>{
            if(data){
                setConfusionMatrix(data);
                setIsPending(false);
            }else{
                info_timer = setTimeout(() => {getConfusionMatrix(abortConstant)}, 5000);
            }
        })
        .catch((err) =>{
            setIsPending(false);
            if(err.name !== 'AbortError'){}
        })        
    };

    return (
        <div className="confusionmatrix">
            <Accordion defaultActiveKey="0">
                <Card bg='white' style={cardCSS}>
                    <Card.Header style={cardHeaderCSS}>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Confusion Matrix
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body style={{padding:'20px'}}>
                        {(isPending && <Spinner show={isPending}/>)}
                        {(!isPending && 
                            <div style={{display:'flex', justifyContent:'center', alignItem:'center', margin:'15px'}}>
                                <table style={{padding:'10px', textAlign: 'center'}}>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td style={{padding:'10px', border: '1px solid black'}}>Treatment:<br/><b>Missing</b></td>
                                            <td style={{padding:'10px', border: '1px solid black'}}>Treatment:<br/><b>Present</b></td>
                                        </tr>
                                        <tr>
                                            <td style={{padding:'10px', border: '1px solid black'}}>Outcome:<br/><b>Negative</b></td>
                                            <td style={{border: '1px solid black'}}>{confusionMatrix && confusionMatrix.length > 0 ? confusionMatrix[0][0] : 0}</td>
                                            <td style={{border: '1px solid black'}}>{confusionMatrix && confusionMatrix.length > 0 ? confusionMatrix[0][1] : 0}</td>
                                        </tr>
                                        <tr>
                                            <td style={{padding:'10px', border: '1px solid black'}}>Outcome:<br/><b>Positive</b></td>
                                            <td style={{border: '1px solid black'}}>{confusionMatrix && confusionMatrix.length > 0 ? confusionMatrix[1][0] : 0}</td>
                                            <td style={{border: '1px solid black'}}>{confusionMatrix && confusionMatrix.length > 0 ? confusionMatrix[1][1] : 0}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        )}
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        </div>
    );
})
  
export default ConfusionMatrix;