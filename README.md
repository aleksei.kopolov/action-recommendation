# Action Recommendation

Discovering Action Recommendation Rules to Enhance Business Process Performance


## Running the aplication
In order to run the application on your local machine you need to have [Docker](https://docs.docker.com/engine/install/) and [Docker-Compose](https://docs.docker.com/compose/install/) to be installed 
on your machine.


After the installation navigate to project root folder [pyrule](https://gitlab.com/aleksei.kopolov/action-recommendation/-/tree/master/pyrule). 
Incide the foldere there will be a file by the name of [docker-compose.yml](https://gitlab.com/aleksei.kopolov/action-recommendation/-/blob/master/pyrule/docker-compose.yml), 
this file contains the configuration of all of the containers. In order to execute the container building process, type in the following command in your terminal: 

```docker-compose up```

This will first download all the dependecies and then build 5 containers: React, FastApi, Mongodb, Rabbitmq and Celery Worker. 
Make sure that you have no applications currently running on the following default ports:
* 3000 - React
* 8000 - FastApi
* 5672 - Rabbitmq
* 27017 - Mongodb

If everything is successful you should have an application running on your local machine.
React application is accessible through [localhost:3000](http://localhost:3000)
